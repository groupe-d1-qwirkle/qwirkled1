﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleClass;

namespace QwirkleTest
{
    [TestClass]
    public class TestSac
    {
        [TestMethod]
        public void TestConstructor()
        {
            Sac Sacoche = new Sac();
            Assert.AreEqual(3,Sacoche.GetSac(1,1));
            Sacoche.SetSac(1, 1, 0);
            Assert.AreEqual(0, Sacoche.GetSac(1, 1));
        }
        [TestMethod]
        public void TestIsNotEmpty()
        {
            Sac Sacoche = new Sac();
            Assert.AreEqual(true, Sacoche.IsNotEmpty());
            for (int i=0;i<6;i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    Sacoche.SetSac(i, j, 0);
                }
            }
            Assert.AreEqual(false, Sacoche.IsNotEmpty());
        }
        [TestMethod]
        public void TestPickRandom()
        {
            Sac Sacoche = new Sac();
            Tuiles tuiles = new Tuiles();
            tuiles = Sacoche.PickRandom();
            Assert.AreNotEqual(6, tuiles.Couleur);
            Assert.AreNotEqual(6, tuiles.Forme);
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    Sacoche.SetSac(i, j, 0);
                }
            }
            Tuiles tuiles2 = new Tuiles();
            tuiles2 = Sacoche.PickRandom();
            Assert.IsNull(tuiles2);
        
        }
        [TestMethod]
        public void TestRemoveTuiles()
        {
            Sac Sacoche = new Sac();
            Tuiles tuiles = new Tuiles(3, 3);
            Sacoche.RemoveTuiles(3, 3);
            Assert.AreEqual(2, Sacoche.GetSac(3, 3));
            Sacoche.RemoveTuiles(3, 3);
            Assert.AreEqual(1, Sacoche.GetSac(3, 3));
            Sacoche.RemoveTuiles(3, 3);
            Assert.AreEqual(0, Sacoche.GetSac(3, 3));

        }
        [TestMethod]
        public void TestRemoveTuilesSurcharge()
        {
            Sac Sacoche = new Sac();
            Tuiles tuiles = new Tuiles(3, 3);
            Sacoche.RemoveTuiles(tuiles);
            Assert.AreEqual(2, Sacoche.GetSac(3, 3));
            Sacoche.RemoveTuiles(tuiles);
            Assert.AreEqual(1, Sacoche.GetSac(3, 3));
            Sacoche.RemoveTuiles(tuiles);
            Assert.AreEqual(0, Sacoche.GetSac(3, 3));
        }
        [TestMethod]
        public void TestPutTuiles()
        {
            Sac Sacoche = new Sac();
            Sacoche.SetSac(3, 3, 0);
            Tuiles tuiles = new Tuiles(3, 3);
            Sacoche.PutTuiles(3, 3);
            Assert.AreEqual(1, Sacoche.GetSac(3, 3));
            Sacoche.PutTuiles(3, 3);
            Assert.AreEqual(2, Sacoche.GetSac(3, 3));
            Sacoche.PutTuiles(3, 3);
            Assert.AreEqual(3, Sacoche.GetSac(3, 3));

        }
        [TestMethod]
        public void TestPutTuilesSurcharge()
        {
            Sac sacoche = new Sac();
            Tuiles tuiles = new Tuiles(3,3);
            sacoche.SetSac(3, 3, 0);
            sacoche.PutTuiles(tuiles);
            Assert.AreEqual(1, sacoche.GetSac(3, 3));
            sacoche.PutTuiles(tuiles);
            Assert.AreEqual(2, sacoche.GetSac(3, 3));
            sacoche.PutTuiles(tuiles);
            Assert.AreEqual(3, sacoche.GetSac(3, 3));

        }
        [TestMethod]
        public void TestGetSac()
        {
            Sac Sacoche = new Sac();
            Assert.AreEqual(3, Sacoche.GetSac(3, 3));
            Assert.AreEqual(3, Sacoche.GetSac(4, 4));
            Assert.AreEqual(3, Sacoche.GetSac(3, 4));
        }
        [TestMethod]
        public void TestSetSac()
        {
            Sac Sacoche = new Sac();
            Sacoche.SetSac(3, 3, 1);
            Assert.AreEqual(1, Sacoche.GetSac(3, 3));
            Sacoche.SetSac(3, 3, 2);
            Assert.AreEqual(2, Sacoche.GetSac(3, 3));
        }



    }
}
