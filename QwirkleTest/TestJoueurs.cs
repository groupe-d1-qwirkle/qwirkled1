﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleClass;

namespace QwirkleTest
{
    [TestClass]
    public class TestJoueurs
    {
        [TestMethod]
        public void TestConstructor()
        {
            Joueurs player1 = new Joueurs("Thomas",18, 0);
            Assert.AreEqual("Thomas", player1.GetPseudo());
            Assert.AreEqual(18, player1.GetAge());
            Assert.AreEqual(0, player1.GetScore());
            Joueurs player2 = new Joueurs("Florian", 17, 0);
            Assert.AreEqual("Florian", player2.GetPseudo());
            Assert.AreEqual(17, player2.GetAge());
            Assert.AreEqual(0, player2.GetScore());
        }
        [TestMethod]
        public void TestSetScore()
        {
            Joueurs player1 = new Joueurs("Thomas", 18, 0);
            Assert.AreEqual(0, player1.GetScore());
            player1.IncrementationScore(5);
            Assert.AreEqual(5, player1.GetScore());
            player1.IncrementationScore(10);
            Assert.AreEqual(15, player1.GetScore());
        }
        [TestMethod]
        public void TestTuilesManquantes()
        {
            Sac sacoche = new Sac();
            Joueurs Player = new Joueurs("Thomas",18, 0);
            List<Tuiles> list;
            Tuiles tuile = new Tuiles();
            Player.TuilesManquantes(sacoche);
            list = Player.Getlist();
            Assert.AreNotEqual(0, list.Count());
            Assert.AreNotEqual(1, list.Count());
            Assert.AreNotEqual(2, list.Count());
            Assert.AreNotEqual(3, list.Count());
            Assert.AreNotEqual(4, list.Count());
            Assert.AreNotEqual(5, list.Count());
            Assert.AreEqual(6, list.Count());
        }
    }
}
