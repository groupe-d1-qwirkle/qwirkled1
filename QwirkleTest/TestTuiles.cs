﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleClass;

namespace QwirkleTest
{
    [TestClass]
    public class TestTuiles
    {
        [TestMethod]
        public void TestTuileConstructor()
        {
            // Test constructor de base 
            Tuiles testTuiles = new Tuiles(3,1);
            Assert.AreEqual(3, testTuiles.Couleur);
            Assert.AreEqual(1, testTuiles.Forme);

            // Test constructor en cas de données non valide

            Tuiles testErrorTuiles = new Tuiles(8, 9);
            Assert.AreEqual(6, testErrorTuiles.Couleur);
            Assert.AreEqual(6, testErrorTuiles.Forme);
        }
        [TestMethod]
        public void GetHashCodeTest() //vérifie le test du generateur de GetHashCode
        {
            Tuiles TuileA = new Tuiles(0, 0);
            Tuiles TuileB = new Tuiles(2, 5);
            Tuiles TuileC = new Tuiles(0, 2);
            Tuiles TuileNull = new Tuiles();
            Assert.AreEqual(0, TuileA.GetHashCode());
            Assert.AreEqual(25, TuileB.GetHashCode());
            Assert.AreEqual(2, TuileC.GetHashCode());
            Assert.AreEqual(66, TuileNull.GetHashCode());
        }
        [TestMethod]
        public void TestAreEqual() // verifie la surcharge de == 
        {

            Tuiles tuile1 = new Tuiles(0, 0);
            Tuiles tuile2 = new Tuiles(2, 5);
            Tuiles tuile3 = new Tuiles(0, 0);
            Assert.AreEqual(true, tuile1 == tuile3);
            Assert.AreEqual(false, tuile1 == tuile2);
        }
        [TestMethod]
        public void TestAreNotEqual() // verifie la surcharge de !=
        {


            Tuiles tuile1 = new Tuiles(0, 0);
            Tuiles tuile2 = new Tuiles(2, 5);
            Tuiles tuile3 = new Tuiles(0, 0);
            Assert.AreEqual(false, tuile1 != tuile3);
            Assert.AreEqual(true, tuile1 != tuile2);
        }
        [TestMethod]
        public void TestEqual() //vérifie la surcharge de Equal
        {
            Tuiles tuile1 = new Tuiles(0, 0);
            Tuiles tuile2 = new Tuiles(2, 5);
            Tuiles tuile3 = new Tuiles(0, 0);
            Assert.AreEqual(true, tuile1.Equals(tuile3));
            Assert.AreEqual(false, tuile1.Equals(tuile2));
        }
    }
}
