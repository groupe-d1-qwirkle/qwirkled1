﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleClass;

namespace QwirkleTest
{
    [TestClass]
    public class TestPlateau
    {
        //Certains methode n'ont pas de test car je la réalisation de ces test etait trop complexe.
        [TestMethod]
        public void TestConstructor()
        {
            //Test du construteur et verification des valeurs
            Plateau plateau = new Plateau();
            Tuiles tuileA = new Tuiles();
            Assert.AreEqual(3, plateau.LargeurPlateau);
            Assert.AreEqual(3, plateau.HauteurPlateau);
            Assert.AreEqual(3, plateau.LargeurGestion);
            Assert.AreEqual(3, plateau.HauteurGestion);
            Assert.AreEqual(0, plateau.GetGestion(1, 1));
        }
        [TestMethod]

        public void TestGetPlateau()
        {
            Plateau plateau = new Plateau();
            Tuiles tuileFalse = new Tuiles();
            Assert.AreEqual(tuileFalse, plateau.GetPlateau(1, 1));
        }
        public void TestGetGestion()
        {
            Plateau plateau = new Plateau();
            Assert.AreEqual(0, plateau.GetGestion(1, 1));
        }
        [TestMethod]

        public void TestSetGestion()
        {
            Plateau plateau = new Plateau();
            Assert.AreEqual(0, plateau.GetGestion(1, 1));
            plateau.SetGestion(1, 1, 1);
            Assert.AreEqual(1, plateau.GetGestion(1, 1));
        }
        [TestMethod]

        public void TestSetPlateau()
        {
            Plateau plateau = new Plateau();
            Tuiles tuileA = new Tuiles(1, 1);
            plateau.SetPlateau(1, 1, tuileA);
            Assert.AreEqual(tuileA, plateau.GetPlateau(1, 1));
        }
         [TestMethod]
        public void TestAddColonne()
        {
            // Placement d'une tuile sur le plateau puis ajout d'une colonne.
            // il s'agit de vérifié si la tuile a bien été repositioné
            bool fin = true;
            Plateau plateau = new Plateau();
            Tuiles tuileA = new Tuiles(1, 1);
            Tuiles tuileFalse = new Tuiles();
            plateau.SetPlateau(2, 2, tuileA);
            plateau.AddColonne(fin);
            Assert.AreEqual(tuileA, plateau.GetPlateau(2, 2));
            Assert.AreEqual(tuileFalse, plateau.GetPlateau(3, 2));
            Assert.AreEqual(0, plateau.GetGestion(2, 2));
            Assert.AreEqual(0, plateau.GetGestion(3, 2));
            fin = false;
            plateau.SetPlateau(0, 0, tuileA);
            plateau.AddColonne(fin);
            Assert.AreEqual(tuileFalse, plateau.GetPlateau(0, 0));
            Assert.AreEqual(tuileA, plateau.GetPlateau(1, 0));
            Assert.AreEqual(0, plateau.GetGestion(0, 0));
            Assert.AreEqual(0, plateau.GetGestion(1, 0));
        }
        [TestMethod]
        public void TestAddLigne()
        {
            //Même test que pour AddLigne
            bool fin = true;
            Plateau plateau = new Plateau();
            Tuiles tuileA = new Tuiles(1, 1);
            Tuiles tuileFalse = new Tuiles();
            plateau.SetPlateau(2, 2, tuileA);
            plateau.AddLigne(fin);
            Assert.AreEqual(tuileA, plateau.GetPlateau(2, 2));
            Assert.AreEqual(tuileFalse, plateau.GetPlateau(2, 3));
            Assert.AreEqual(0, plateau.GetGestion(2, 2));
            Assert.AreEqual(0, plateau.GetGestion(2, 3));
            plateau.SetPlateau(0, 0, tuileA);
            fin = false;
            plateau.AddLigne(fin);
            Assert.AreEqual(tuileFalse, plateau.GetPlateau(0, 0));
            Assert.AreEqual(tuileA, plateau.GetPlateau(0, 1));
            Assert.AreEqual(0, plateau.GetGestion(0, 0));
            Assert.AreEqual(0, plateau.GetGestion(0, 1));
        }
        [TestMethod]
        public void TestRedimension()
        {
            // Même methode que pour AddLigne et AddColonne mais pour la fonction redimension
            Plateau plateau = new Plateau();
            int colonne = 0;
            int ligne = 0;
            Tuiles tuileA = new Tuiles(1, 1);
            plateau.SetPlateau(2, 2, tuileA);
            plateau.Redimension(colonne, ligne);
            Tuiles tuileFalse = new Tuiles();
            Assert.AreEqual(tuileFalse, plateau.GetPlateau(0, 3));
            Assert.AreEqual(tuileFalse, plateau.GetPlateau(3, 0));
            Assert.AreEqual(tuileA, plateau.GetPlateau(3, 3));
            colonne = 3;
            ligne = 3;
            plateau.Redimension(colonne, ligne);
            Assert.AreEqual(tuileFalse, plateau.GetPlateau(0, 4));
            Assert.AreEqual(tuileFalse, plateau.GetPlateau(4, 4));
            Assert.AreEqual(tuileA, plateau.GetPlateau(3, 3));
        }
        [TestMethod]
        public void TestPositionSelect()
        {
            // Placement d'une tuile a la position 1.1 
            // Verification que la fonction nous renvoi false si on tente de reposer une tuile dessus
            Plateau plateau = new Plateau();
            Tuiles tuileA = new Tuiles(1, 1);
            plateau.SetPlateau(1, 1, tuileA);
            plateau.PositionSelect(1, 1);
            Assert.AreEqual(false, plateau.Valide);
        }
        [TestMethod]
        public void TestVerification()
        {
            // placement de tuile et verification si d'autre tuile peuvent être posé ou non 
            Plateau plateau = new Plateau();
            Tuiles tuile = new Tuiles(1, 1);
            Tuiles tuileA = new Tuiles(1, 2);
            Tuiles tuileB = new Tuiles(1, 3);
            Tuiles tuileC = new Tuiles(2, 1);
            Tuiles tuileD = new Tuiles(2, 1);
            Tuiles tuileFalse = new Tuiles(1, 1);
            plateau.Redimension(0, 0);
            plateau.Redimension(3, 3);
            plateau.Redimension(0, 0);
            plateau.Redimension(5, 5);
            plateau.SetPlateau(2, 2, tuile);
            plateau.VerificationCote(2, 1, tuileA);
            Assert.AreEqual(true, plateau.Valide);
            plateau.VerificationCote(3, 2, tuileB);
            Assert.AreEqual(true, plateau.Valide);
            plateau.VerificationCote(2, 3, tuileC);
            Assert.AreEqual(true, plateau.Valide);
            plateau.VerificationCote(1, 2, tuileD);
            Assert.AreEqual(true, plateau.Valide);
            plateau.VerificationCote(1, 2, tuileFalse);
            Assert.AreEqual(false, plateau.Valide);
        }
        [TestMethod]
        public void TestAddTuile()
        {
            // Verification complete du test AddTuile
            Plateau plateau = new Plateau();
            Tuiles tuile = new Tuiles(1, 1);
            Tuiles tuileA = new Tuiles(1, 2);
            Tuiles tuileB = new Tuiles(1, 4);
            Tuiles tuileFalse = new Tuiles(1, 1);
            Assert.AreEqual(true, plateau.AddTuile(1, 1, tuile));
            Assert.AreEqual(true, plateau.AddTuile(1, 0, tuileA));
            Assert.AreEqual(false, plateau.AddTuile(2, 2, tuileB));
            Assert.AreEqual(false, plateau.AddTuile(0, 2, tuileFalse));
        }
        [TestMethod]
        public void TestArchiveTuile()
        {
            // on verifie si après avoir lancé la fonction , la valeur passe bien a 1
            Plateau plateau = new Plateau();
            Tuiles tuileA = new Tuiles(1, 0);
            plateau.AddTuile(1, 1, tuileA);
            Assert.AreEqual(2, plateau.GetGestion(1, 1));
            plateau.ArchiveTuile();
            Assert.AreEqual(1, plateau.GetGestion(1, 1));
        }
        [TestMethod]
        public void TestCalcul()
        {
            // Nous simulons une pose de tuile et comptons les pointts
            Plateau plateau = new Plateau();
            Tuiles tuileA = new Tuiles(1, 1);
            Tuiles tuileB = new Tuiles(1, 2);
            Tuiles tuileC = new Tuiles(1, 3);
            Tuiles tuileD = new Tuiles(1, 4);
            Tuiles tuileE = new Tuiles(1, 5);
            Tuiles tuileF = new Tuiles(1, 0);
            plateau.AddTuile(1, 1, tuileA);
            plateau.AddTuile(2, 1, tuileB);
            plateau.AddTuile(3, 1, tuileC);
            plateau.AddTuile(4, 1, tuileD);
            plateau.AddTuile(5, 1, tuileE);
            plateau.AddTuile(6, 1, tuileF);
            Assert.AreEqual(6, plateau.ScoreCalcul());
            plateau.ArchiveTuile();
            plateau.AddTuile(1, 2, tuileB);
            Assert.AreEqual(2, plateau.ScoreCalcul());
        }
        [TestMethod]
        public void ScoreQwirkle()
        {
            //Simulation d'un Qwirkle
            Plateau plateau = new Plateau();
            Tuiles tuileA = new Tuiles(1, 0);
            Tuiles tuileB = new Tuiles(1, 1);
            Tuiles tuileC = new Tuiles(1, 2);
            Tuiles tuileD = new Tuiles(1, 3);
            Tuiles tuileE = new Tuiles(1, 4);
            Tuiles tuileF = new Tuiles(1, 5);
            plateau.AddTuile(1, 1, tuileA);
            plateau.AddTuile(2, 1, tuileB);
            plateau.AddTuile(3, 1, tuileC);
            plateau.AddTuile(4, 1, tuileD);
            plateau.AddTuile(5, 1, tuileE);
            plateau.AddTuile(6, 1, tuileF);
            Assert.AreEqual(6, plateau.ScoreQwirkle());
            plateau.ArchiveTuile();
            plateau.AddTuile(1, 2, tuileB);
            plateau.AddTuile(1, 3, tuileC);
            plateau.AddTuile(1, 4, tuileD);
            plateau.AddTuile(1, 5, tuileE);
            plateau.AddTuile(1, 6, tuileF);
            Assert.AreEqual(6, plateau.ScoreQwirkle());
        }    
        [TestMethod]
        public void TestVerificationTuileUnique()
        {
            // Nous posons une tuile et tentons de reposer exactement la même a coté
            Plateau plateau = new Plateau();
            Tuiles tuileA = new Tuiles(1, 1);
            Tuiles tuileB = new Tuiles(1, 2);
            Tuiles tuileC = new Tuiles(1, 2);
            plateau.AddTuile(1, 1, tuileA);
            plateau.AddTuile(1, 2, tuileB);
            plateau.Redimension(1, 3);
            plateau.VerificationTuileUnique(1, 3, tuileC);
            Assert.AreEqual(false, plateau.Valide);
        }
        [TestMethod]
        public void TestVerificationIdentique()
        {
            // verification qu'il n'y a pas de mélange de tuile avec une simulation de pose 
            Plateau plateau = new Plateau();
            Tuiles tuileA = new Tuiles(1, 1);
            Tuiles tuileB = new Tuiles(1, 2);
            Tuiles tuileC = new Tuiles(2, 2);
            plateau.AddTuile(1, 1, tuileA);
            plateau.AddTuile(1, 2, tuileB);
            plateau.Redimension(1, 3);
            plateau.VerificationIdentique(1, 3, tuileC);
            Assert.AreEqual(false, plateau.Valide);
        }
        [TestMethod]
        public void TestResetBool()
        {
            // Verification si les bool sont bien reset
            Plateau plateau = new Plateau();
            plateau.ResetBool();
            Assert.AreEqual(true, plateau.DeuxiemeTour);
            Assert.AreEqual(false, plateau.DeuxiemePoseBool);
            Assert.AreEqual(false, plateau.AutrePoseBool);
        }
        [TestMethod]
        public void TestSauvegardePose()
        {
            // Simulation d'une pose et vérification du blocage de lapose
            Plateau plateau = new Plateau();
            Tuiles tuileA = new Tuiles(1, 1);
            Tuiles tuileB = new Tuiles(1, 2);
            plateau.AddTuile(1, 1, tuileA);
            plateau.AddTuile(1, 2, tuileB);
            plateau.SauvegardePose();
            Assert.AreEqual(1, plateau.StockageColonne);
            Plateau plateau2 = new Plateau();
            plateau2.AddTuile(1, 1, tuileA);
            plateau2.AddTuile(2, 1, tuileB);
            plateau2.SauvegardePose();
            Assert.AreEqual(1, plateau2.StockageLigne);
        }
        [TestMethod]
        public void TestResetBoolDimension()
        {
            // Verification si les bool sont bien reset
            Plateau plateau = new Plateau();
            Tuiles tuileA = new Tuiles(1, 1);
            plateau.AddTuile(2, 2, tuileA);
            plateau.ResetBoolDimension();
            Assert.AreEqual(false, plateau.AjoutColonne);
            Assert.AreEqual(false, plateau.AjoutColonneFin);
            Assert.AreEqual(false, plateau.AjoutLigne);
            Assert.AreEqual(false, plateau.AjoutLigneFin);
            Assert.AreEqual(-1, plateau.StockageColonne);
            Assert.AreEqual(-1, plateau.StockageLigne);

        }
    }
}
