﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class plateau_vb
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.LblPionJoueur = New System.Windows.Forms.Label()
        Me.GB_Pion_joueur = New System.Windows.Forms.GroupBox()
        Me.PB5 = New System.Windows.Forms.PictureBox()
        Me.PB4 = New System.Windows.Forms.PictureBox()
        Me.PB3 = New System.Windows.Forms.PictureBox()
        Me.PB2 = New System.Windows.Forms.PictureBox()
        Me.PB1 = New System.Windows.Forms.PictureBox()
        Me.PB0 = New System.Windows.Forms.PictureBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.LblScoreJ4 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.LblScoreJ3 = New System.Windows.Forms.Label()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.BtnMenu = New System.Windows.Forms.Button()
        Me.btn_valideTour = New System.Windows.Forms.Button()
        Me.PictureBox_99_99 = New System.Windows.Forms.PictureBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.LblScoreJ2 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LblScoreJ1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.btn_start = New System.Windows.Forms.Button()
        Me.pnl_plateau = New System.Windows.Forms.Panel()
        Me.btn_stop = New System.Windows.Forms.Button()
        Me.GB_Pion_joueur.SuspendLayout()
        CType(Me.PB5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PB4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PB3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PB2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PB1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PB0, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        CType(Me.PictureBox_99_99, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.SuspendLayout()
        '
        'LblPionJoueur
        '
        Me.LblPionJoueur.AutoSize = True
        Me.LblPionJoueur.Location = New System.Drawing.Point(7, 35)
        Me.LblPionJoueur.Name = "LblPionJoueur"
        Me.LblPionJoueur.Size = New System.Drawing.Size(89, 13)
        Me.LblPionJoueur.TabIndex = 0
        Me.LblPionJoueur.Text = "pion du joueur 1 :"
        '
        'GB_Pion_joueur
        '
        Me.GB_Pion_joueur.Controls.Add(Me.PB5)
        Me.GB_Pion_joueur.Controls.Add(Me.PB4)
        Me.GB_Pion_joueur.Controls.Add(Me.PB3)
        Me.GB_Pion_joueur.Controls.Add(Me.PB2)
        Me.GB_Pion_joueur.Controls.Add(Me.PB1)
        Me.GB_Pion_joueur.Controls.Add(Me.PB0)
        Me.GB_Pion_joueur.Controls.Add(Me.LblPionJoueur)
        Me.GB_Pion_joueur.Location = New System.Drawing.Point(12, 19)
        Me.GB_Pion_joueur.Name = "GB_Pion_joueur"
        Me.GB_Pion_joueur.Size = New System.Drawing.Size(446, 77)
        Me.GB_Pion_joueur.TabIndex = 9
        Me.GB_Pion_joueur.TabStop = False
        '
        'PB5
        '
        Me.PB5.Location = New System.Drawing.Point(382, 19)
        Me.PB5.Name = "PB5"
        Me.PB5.Size = New System.Drawing.Size(50, 50)
        Me.PB5.TabIndex = 6
        Me.PB5.TabStop = False
        '
        'PB4
        '
        Me.PB4.Location = New System.Drawing.Point(326, 19)
        Me.PB4.Name = "PB4"
        Me.PB4.Size = New System.Drawing.Size(50, 50)
        Me.PB4.TabIndex = 5
        Me.PB4.TabStop = False
        '
        'PB3
        '
        Me.PB3.Location = New System.Drawing.Point(270, 19)
        Me.PB3.Name = "PB3"
        Me.PB3.Size = New System.Drawing.Size(50, 50)
        Me.PB3.TabIndex = 4
        Me.PB3.TabStop = False
        '
        'PB2
        '
        Me.PB2.Location = New System.Drawing.Point(214, 19)
        Me.PB2.Name = "PB2"
        Me.PB2.Size = New System.Drawing.Size(50, 50)
        Me.PB2.TabIndex = 3
        Me.PB2.TabStop = False
        '
        'PB1
        '
        Me.PB1.Location = New System.Drawing.Point(158, 19)
        Me.PB1.Name = "PB1"
        Me.PB1.Size = New System.Drawing.Size(50, 50)
        Me.PB1.TabIndex = 2
        Me.PB1.TabStop = False
        '
        'PB0
        '
        Me.PB0.Location = New System.Drawing.Point(102, 19)
        Me.PB0.Name = "PB0"
        Me.PB0.Size = New System.Drawing.Size(50, 50)
        Me.PB0.TabIndex = 1
        Me.PB0.TabStop = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.Label11)
        Me.GroupBox4.Controls.Add(Me.LblScoreJ4)
        Me.GroupBox4.Location = New System.Drawing.Point(582, 16)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(194, 28)
        Me.GroupBox4.TabIndex = 4
        Me.GroupBox4.TabStop = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(114, 11)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(35, 13)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "points"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(95, 11)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(13, 13)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "0"
        '
        'LblScoreJ4
        '
        Me.LblScoreJ4.AutoSize = True
        Me.LblScoreJ4.Location = New System.Drawing.Point(7, 11)
        Me.LblScoreJ4.Name = "LblScoreJ4"
        Me.LblScoreJ4.Size = New System.Drawing.Size(82, 13)
        Me.LblScoreJ4.TabIndex = 0
        Me.LblScoreJ4.Text = "Score joueur 4 :"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.LblScoreJ3)
        Me.GroupBox3.Location = New System.Drawing.Point(388, 16)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(194, 28)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(114, 11)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(35, 13)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "points"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(95, 11)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(13, 13)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "0"
        '
        'LblScoreJ3
        '
        Me.LblScoreJ3.AutoSize = True
        Me.LblScoreJ3.Location = New System.Drawing.Point(7, 11)
        Me.LblScoreJ3.Name = "LblScoreJ3"
        Me.LblScoreJ3.Size = New System.Drawing.Size(82, 13)
        Me.LblScoreJ3.TabIndex = 0
        Me.LblScoreJ3.Text = "Score joueur 3 :"
        '
        'GroupBox7
        '
        Me.GroupBox7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox7.Controls.Add(Me.BtnMenu)
        Me.GroupBox7.Controls.Add(Me.btn_valideTour)
        Me.GroupBox7.Controls.Add(Me.PictureBox_99_99)
        Me.GroupBox7.Location = New System.Drawing.Point(681, 19)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(175, 77)
        Me.GroupBox7.TabIndex = 10
        Me.GroupBox7.TabStop = False
        '
        'BtnMenu
        '
        Me.BtnMenu.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnMenu.BackgroundImage = Global.Qwirkle.My.Resources.Resources.menu
        Me.BtnMenu.Location = New System.Drawing.Point(120, 19)
        Me.BtnMenu.Name = "BtnMenu"
        Me.BtnMenu.Size = New System.Drawing.Size(50, 50)
        Me.BtnMenu.TabIndex = 11
        Me.BtnMenu.UseVisualStyleBackColor = True
        '
        'btn_valideTour
        '
        Me.btn_valideTour.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_valideTour.BackgroundImage = Global.Qwirkle.My.Resources.Resources.okay
        Me.btn_valideTour.Location = New System.Drawing.Point(8, 19)
        Me.btn_valideTour.Name = "btn_valideTour"
        Me.btn_valideTour.Size = New System.Drawing.Size(50, 50)
        Me.btn_valideTour.TabIndex = 10
        Me.btn_valideTour.UseVisualStyleBackColor = True
        Me.btn_valideTour.Visible = False
        '
        'PictureBox_99_99
        '
        Me.PictureBox_99_99.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox_99_99.BackgroundImage = Global.Qwirkle.My.Resources.Resources.swap
        Me.PictureBox_99_99.Location = New System.Drawing.Point(64, 19)
        Me.PictureBox_99_99.Name = "PictureBox_99_99"
        Me.PictureBox_99_99.Size = New System.Drawing.Size(50, 50)
        Me.PictureBox_99_99.TabIndex = 8
        Me.PictureBox_99_99.TabStop = False
        Me.PictureBox_99_99.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(95, 11)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(13, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "0"
        '
        'LblScoreJ2
        '
        Me.LblScoreJ2.AutoSize = True
        Me.LblScoreJ2.Location = New System.Drawing.Point(7, 11)
        Me.LblScoreJ2.Name = "LblScoreJ2"
        Me.LblScoreJ2.Size = New System.Drawing.Size(82, 13)
        Me.LblScoreJ2.TabIndex = 0
        Me.LblScoreJ2.Text = "Score joueur 2 :"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.LblScoreJ2)
        Me.GroupBox2.Location = New System.Drawing.Point(194, 16)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(194, 28)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(114, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "points"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(114, 11)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "points"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(95, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(13, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "0"
        '
        'LblScoreJ1
        '
        Me.LblScoreJ1.AutoSize = True
        Me.LblScoreJ1.Location = New System.Drawing.Point(7, 11)
        Me.LblScoreJ1.Name = "LblScoreJ1"
        Me.LblScoreJ1.Size = New System.Drawing.Size(82, 13)
        Me.LblScoreJ1.TabIndex = 0
        Me.LblScoreJ1.Text = "Score joueur 1 :"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.LblScoreJ1)
        Me.GroupBox1.Location = New System.Drawing.Point(0, 16)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(194, 28)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.GroupBox4)
        Me.GroupBox5.Controls.Add(Me.GroupBox3)
        Me.GroupBox5.Controls.Add(Me.GroupBox2)
        Me.GroupBox5.Controls.Add(Me.GroupBox1)
        Me.GroupBox5.Location = New System.Drawing.Point(6, 12)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(776, 50)
        Me.GroupBox5.TabIndex = 7
        Me.GroupBox5.TabStop = False
        '
        'GroupBox6
        '
        Me.GroupBox6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox6.AutoSize = True
        Me.GroupBox6.Controls.Add(Me.btn_stop)
        Me.GroupBox6.Controls.Add(Me.btn_start)
        Me.GroupBox6.Controls.Add(Me.GroupBox7)
        Me.GroupBox6.Controls.Add(Me.GB_Pion_joueur)
        Me.GroupBox6.Location = New System.Drawing.Point(16, 505)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(862, 115)
        Me.GroupBox6.TabIndex = 9
        Me.GroupBox6.TabStop = False
        '
        'btn_start
        '
        Me.btn_start.Location = New System.Drawing.Point(476, 38)
        Me.btn_start.Name = "btn_start"
        Me.btn_start.Size = New System.Drawing.Size(90, 50)
        Me.btn_start.TabIndex = 11
        Me.btn_start.Text = "Let's GO! "
        Me.btn_start.UseVisualStyleBackColor = True
        '
        'pnl_plateau
        '
        Me.pnl_plateau.AutoScroll = True
        Me.pnl_plateau.Location = New System.Drawing.Point(130, 109)
        Me.pnl_plateau.Name = "pnl_plateau"
        Me.pnl_plateau.Size = New System.Drawing.Size(669, 390)
        Me.pnl_plateau.TabIndex = 10
        '
        'btn_stop
        '
        Me.btn_stop.Location = New System.Drawing.Point(572, 38)
        Me.btn_stop.Name = "btn_stop"
        Me.btn_stop.Size = New System.Drawing.Size(89, 50)
        Me.btn_stop.TabIndex = 12
        Me.btn_stop.Text = "Stopper le jeu"
        Me.btn_stop.UseVisualStyleBackColor = True
        Me.btn_stop.Visible = False
        '
        'plateau_vb
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(956, 632)
        Me.Controls.Add(Me.pnl_plateau)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox6)
        Me.Name = "plateau_vb"
        Me.Text = "plateau"
        Me.GB_Pion_joueur.ResumeLayout(False)
        Me.GB_Pion_joueur.PerformLayout()
        CType(Me.PB5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PB4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PB3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PB2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PB1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PB0, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        CType(Me.PictureBox_99_99, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PB5 As PictureBox
    Friend WithEvents PB4 As PictureBox
    Friend WithEvents PB3 As PictureBox
    Friend WithEvents PB2 As PictureBox
    Friend WithEvents PB1 As PictureBox
    Friend WithEvents LblPionJoueur As Label
    Friend WithEvents GB_Pion_joueur As GroupBox
    Friend WithEvents PB0 As PictureBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents LblScoreJ4 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents LblScoreJ3 As Label
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents Label5 As Label
    Friend WithEvents LblScoreJ2 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents LblScoreJ1 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents pnl_plateau As Panel
    Friend WithEvents btn_start As Button
    Friend WithEvents btn_valideTour As Button
    Friend WithEvents BtnMenu As Button
    Friend WithEvents PictureBox_99_99 As PictureBox
    Friend WithEvents btn_stop As Button
End Class
