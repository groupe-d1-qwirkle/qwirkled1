﻿Public Class accueil
    Private Sub Btn_Jouer_Click(sender As Object, e As EventArgs) Handles Btn_Jouer.Click
        Nbr_de_joueurs.Show()
    End Sub

    Private Sub Btn_quitter_Click(sender As Object, e As EventArgs) Handles Btn_quitter.Click
        Me.Close()
    End Sub

    Private Sub Btn_regles_Click(sender As Object, e As EventArgs) Handles Btn_regles.Click
        Regle.Show()
        Me.Hide()
    End Sub

    Private Sub Btn_Option_Click(sender As Object, e As EventArgs) Handles Btn_Option.Click
        Options.Show()
        Me.Hide()
    End Sub

    Private Sub Accueil_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        PictureBox1.Left = ClientSize.Width / 2 - PictureBox1.Width / 2
        GroupBox1.Left = ClientSize.Width / 2 - GroupBox1.Width / 2
        PictureBox1.Top = ClientSize.Height / 2 - PictureBox1.Height / 2 - 50
        GroupBox1.Top = ClientSize.Height / 2 + PictureBox1.Height - 80
    End Sub

    Private Sub Accueil_Load(sender As Object, e As EventArgs) Handles Me.Load
        PictureBox1.Left = ClientSize.Width / 2 - PictureBox1.Width / 2
        GroupBox1.Left = ClientSize.Width / 2 - GroupBox1.Width / 2
        PictureBox1.Top = ClientSize.Height / 2 - PictureBox1.Height / 2 - 50
        GroupBox1.Top = ClientSize.Height / 2 + PictureBox1.Height - 80
    End Sub
End Class
