﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class accueil
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Btn_Jouer = New System.Windows.Forms.Button()
        Me.Btn_regles = New System.Windows.Forms.Button()
        Me.Btn_Option = New System.Windows.Forms.Button()
        Me.Btn_quitter = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Btn_Jouer
        '
        Me.Btn_Jouer.Location = New System.Drawing.Point(0, 5)
        Me.Btn_Jouer.Name = "Btn_Jouer"
        Me.Btn_Jouer.Size = New System.Drawing.Size(100, 40)
        Me.Btn_Jouer.TabIndex = 1
        Me.Btn_Jouer.Text = "Jouer"
        Me.Btn_Jouer.UseVisualStyleBackColor = True
        '
        'Btn_regles
        '
        Me.Btn_regles.Location = New System.Drawing.Point(150, 5)
        Me.Btn_regles.Name = "Btn_regles"
        Me.Btn_regles.Size = New System.Drawing.Size(100, 40)
        Me.Btn_regles.TabIndex = 2
        Me.Btn_regles.Text = "Regles"
        Me.Btn_regles.UseVisualStyleBackColor = True
        '
        'Btn_Option
        '
        Me.Btn_Option.Location = New System.Drawing.Point(300, 5)
        Me.Btn_Option.Name = "Btn_Option"
        Me.Btn_Option.Size = New System.Drawing.Size(100, 40)
        Me.Btn_Option.TabIndex = 3
        Me.Btn_Option.Text = "Options"
        Me.Btn_Option.UseVisualStyleBackColor = True
        '
        'Btn_quitter
        '
        Me.Btn_quitter.Location = New System.Drawing.Point(450, 5)
        Me.Btn_quitter.Name = "Btn_quitter"
        Me.Btn_quitter.Size = New System.Drawing.Size(100, 40)
        Me.Btn_quitter.TabIndex = 4
        Me.Btn_quitter.Text = "Quitter"
        Me.Btn_quitter.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Qwirkle.My.Resources.Resources.Qwirkle
        Me.PictureBox1.Location = New System.Drawing.Point(64, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(656, 196)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Btn_quitter)
        Me.GroupBox1.Controls.Add(Me.Btn_Jouer)
        Me.GroupBox1.Controls.Add(Me.Btn_regles)
        Me.GroupBox1.Controls.Add(Me.Btn_Option)
        Me.GroupBox1.Location = New System.Drawing.Point(50, 310)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(550, 44)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        '
        'accueil
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "accueil"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Btn_Jouer As Button
    Friend WithEvents Btn_regles As Button
    Friend WithEvents Btn_Option As Button
    Friend WithEvents Btn_quitter As Button
    Friend WithEvents GroupBox1 As GroupBox
End Class
