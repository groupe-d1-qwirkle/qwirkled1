﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Regle
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_regle = New System.Windows.Forms.Label()
        Me.btn_next = New System.Windows.Forms.Button()
        Me.btn_return = New System.Windows.Forms.Button()
        Me.btn_precedent = New System.Windows.Forms.Button()
        Me.pb_photo4 = New System.Windows.Forms.PictureBox()
        Me.pb_photo3 = New System.Windows.Forms.PictureBox()
        Me.pb_photo2 = New System.Windows.Forms.PictureBox()
        Me.pb_photo1 = New System.Windows.Forms.PictureBox()
        CType(Me.pb_photo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pb_photo3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pb_photo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pb_photo1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbl_regle
        '
        Me.lbl_regle.AutoSize = True
        Me.lbl_regle.Font = New System.Drawing.Font("Microsoft Sans Serif", 48.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_regle.ForeColor = System.Drawing.Color.IndianRed
        Me.lbl_regle.Location = New System.Drawing.Point(293, 11)
        Me.lbl_regle.Name = "lbl_regle"
        Me.lbl_regle.Size = New System.Drawing.Size(232, 73)
        Me.lbl_regle.TabIndex = 0
        Me.lbl_regle.Text = "Règles"
        '
        'btn_next
        '
        Me.btn_next.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_next.ForeColor = System.Drawing.Color.IndianRed
        Me.btn_next.Location = New System.Drawing.Point(636, 650)
        Me.btn_next.Name = "btn_next"
        Me.btn_next.Size = New System.Drawing.Size(100, 30)
        Me.btn_next.TabIndex = 5
        Me.btn_next.Text = "Suivant"
        Me.btn_next.UseVisualStyleBackColor = True
        '
        'btn_return
        '
        Me.btn_return.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_return.ForeColor = System.Drawing.Color.IndianRed
        Me.btn_return.Location = New System.Drawing.Point(361, 650)
        Me.btn_return.Name = "btn_return"
        Me.btn_return.Size = New System.Drawing.Size(100, 30)
        Me.btn_return.TabIndex = 6
        Me.btn_return.Text = "Retour"
        Me.btn_return.UseVisualStyleBackColor = True
        '
        'btn_precedent
        '
        Me.btn_precedent.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_precedent.ForeColor = System.Drawing.Color.IndianRed
        Me.btn_precedent.Location = New System.Drawing.Point(85, 650)
        Me.btn_precedent.Name = "btn_precedent"
        Me.btn_precedent.Size = New System.Drawing.Size(100, 30)
        Me.btn_precedent.TabIndex = 7
        Me.btn_precedent.Text = "Précédent"
        Me.btn_precedent.UseVisualStyleBackColor = True
        '
        'pb_photo4
        '
        Me.pb_photo4.BackgroundImage = Global.Qwirkle.My.Resources.Resources.ImagePage1
        Me.pb_photo4.Location = New System.Drawing.Point(23, 87)
        Me.pb_photo4.Name = "pb_photo4"
        Me.pb_photo4.Size = New System.Drawing.Size(758, 540)
        Me.pb_photo4.TabIndex = 4
        Me.pb_photo4.TabStop = False
        '
        'pb_photo3
        '
        Me.pb_photo3.BackgroundImage = Global.Qwirkle.My.Resources.Resources.ImagePage2
        Me.pb_photo3.Location = New System.Drawing.Point(23, 87)
        Me.pb_photo3.Name = "pb_photo3"
        Me.pb_photo3.Size = New System.Drawing.Size(758, 540)
        Me.pb_photo3.TabIndex = 3
        Me.pb_photo3.TabStop = False
        '
        'pb_photo2
        '
        Me.pb_photo2.BackgroundImage = Global.Qwirkle.My.Resources.Resources.ImagePage3
        Me.pb_photo2.Location = New System.Drawing.Point(23, 87)
        Me.pb_photo2.Name = "pb_photo2"
        Me.pb_photo2.Size = New System.Drawing.Size(758, 540)
        Me.pb_photo2.TabIndex = 2
        Me.pb_photo2.TabStop = False
        '
        'pb_photo1
        '
        Me.pb_photo1.BackgroundImage = Global.Qwirkle.My.Resources.Resources.ImagePage4
        Me.pb_photo1.Location = New System.Drawing.Point(23, 87)
        Me.pb_photo1.Name = "pb_photo1"
        Me.pb_photo1.Size = New System.Drawing.Size(758, 540)
        Me.pb_photo1.TabIndex = 1
        Me.pb_photo1.TabStop = False
        '
        'Regle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(806, 712)
        Me.Controls.Add(Me.btn_precedent)
        Me.Controls.Add(Me.btn_return)
        Me.Controls.Add(Me.btn_next)
        Me.Controls.Add(Me.pb_photo4)
        Me.Controls.Add(Me.pb_photo3)
        Me.Controls.Add(Me.pb_photo2)
        Me.Controls.Add(Me.pb_photo1)
        Me.Controls.Add(Me.lbl_regle)
        Me.Name = "Regle"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Regle"
        CType(Me.pb_photo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pb_photo3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pb_photo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pb_photo1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_regle As Label
    Friend WithEvents pb_photo1 As PictureBox
    Friend WithEvents pb_photo2 As PictureBox
    Friend WithEvents pb_photo3 As PictureBox
    Friend WithEvents pb_photo4 As PictureBox
    Friend WithEvents btn_next As Button
    Friend WithEvents btn_return As Button
    Friend WithEvents btn_precedent As Button
End Class
