﻿Public Class Regle
    Private compteur As Integer
    Private Sub Btn_return_Click(sender As Object, e As EventArgs) Handles btn_return.Click
        Me.Close()
        accueil.Show()
    End Sub

    Private Sub Btn_next_Click(sender As Object, e As EventArgs) Handles btn_next.Click
        compteur = compteur + 1

        If (compteur = 1) Then
            pb_photo4.Visible = False
            btn_precedent.Visible = True
        End If
        If (compteur = 2) Then
            pb_photo3.Visible = False
        End If
        If (compteur = 3) Then
            pb_photo2.Visible = False
            btn_next.Visible = False
        End If

    End Sub

    Private Sub Btn_precedent_Click(sender As Object, e As EventArgs) Handles btn_precedent.Click
        compteur = compteur - 1

        If (compteur = 0) Then
            pb_photo4.Visible = True
            btn_precedent.Visible = False
        End If
        If (compteur = 1) Then
            pb_photo3.Visible = True
        End If
        If (compteur = 2) Then
            pb_photo2.Visible = True
            btn_next.Visible = True
        End If

    End Sub

    Private Sub Regle_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        btn_precedent.Visible = False

        lbl_regle.Left = ClientSize.Width / 2 - lbl_regle.Width / 2
        pb_photo1.Left = ClientSize.Width / 2 - pb_photo1.Width / 2
        pb_photo2.Left = ClientSize.Width / 2 - pb_photo2.Width / 2
        pb_photo3.Left = ClientSize.Width / 2 - pb_photo3.Width / 2
        pb_photo4.Left = ClientSize.Width / 2 - pb_photo4.Width / 2

        pb_photo1.Top = ClientSize.Height / 2 - pb_photo1.Height / 2
        pb_photo2.Top = ClientSize.Height / 2 - pb_photo2.Height / 2
        pb_photo3.Top = ClientSize.Height / 2 - pb_photo3.Height / 2
        pb_photo4.Top = ClientSize.Height / 2 - pb_photo4.Height / 2

        btn_precedent.Left = pb_photo1.Left
        btn_return.Left = ClientSize.Width / 2 - btn_return.Width / 2
        btn_next.Left = ClientSize.Width / 2 + (btn_return.Left - btn_precedent.Left - btn_next.Width / 2)

        btn_precedent.Top = pb_photo1.Top + pb_photo1.Height + 10
        btn_return.Top = pb_photo1.Top + pb_photo1.Height + 10
        btn_next.Top = pb_photo1.Top + pb_photo1.Height + 10

    End Sub

    Private Sub Regle_Resize(sender As Object, e As EventArgs) Handles Me.Resize

        lbl_regle.Left = ClientSize.Width / 2 - lbl_regle.Width / 2
        pb_photo1.Left = ClientSize.Width / 2 - pb_photo1.Width / 2
        pb_photo2.Left = ClientSize.Width / 2 - pb_photo2.Width / 2
        pb_photo3.Left = ClientSize.Width / 2 - pb_photo3.Width / 2
        pb_photo4.Left = ClientSize.Width / 2 - pb_photo4.Width / 2

        pb_photo1.Top = ClientSize.Height / 2 - pb_photo1.Height / 2
        pb_photo2.Top = ClientSize.Height / 2 - pb_photo2.Height / 2
        pb_photo3.Top = ClientSize.Height / 2 - pb_photo3.Height / 2
        pb_photo4.Top = ClientSize.Height / 2 - pb_photo4.Height / 2

        btn_precedent.Left = pb_photo1.Left
        btn_return.Left = ClientSize.Width / 2 - btn_return.Width / 2
        btn_next.Left = ClientSize.Width / 2 + (btn_return.Left - btn_precedent.Left - btn_next.Width / 2)

        btn_precedent.Top = pb_photo1.Top + pb_photo1.Height + 10
        btn_return.Top = pb_photo1.Top + pb_photo1.Height + 10
        btn_next.Top = pb_photo1.Top + pb_photo1.Height + 10

    End Sub
End Class