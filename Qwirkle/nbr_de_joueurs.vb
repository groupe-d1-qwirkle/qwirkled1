﻿Imports QwirkleClass

Public Class Nbr_de_joueurs
    Public j1 As Joueurs
    Public j2 As Joueurs
    Public j3 As Joueurs
    Public j4 As Joueurs
    Public joueur2 As Boolean
    Public joueur4 As Boolean
    Public joueur3 As Boolean


    Private Sub Nbr_de_joueurs_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        pl_2j.Visible = False
        pl_3j.Visible = False
        pl_4j.Visible = False
        accueil.Hide()
        'Permet de remplir les combobox avec un chiffre de 1 à 99'
        For i As Integer = 1 To 99
            comB_j1_2j.Items.Add(i)
            comB_j2_2j.Items.Add(i)
            comB_j1_3j.Items.Add(i)
            comB_j2_3j.Items.Add(i)
            comB_j3_3j.Items.Add(i)
            comB_j1_4j.Items.Add(i)
            comB_j2_4j.Items.Add(i)
            comB_j3_4j.Items.Add(i)
            comB_j4_4j.Items.Add(i)
        Next

        btn_j2.Left = ClientSize.Width / 3 - pl_2j.Width / 2 - 100
        btn_j3.Left = ClientSize.Width / 2 - pl_3j.Width / 2
        btn_j4.Left = (ClientSize.Width / 3) * 2 - pl_4j.Width / 2 + 100
        pl_2j.Left = ClientSize.Width / 3 - pl_2j.Width / 2 - 100
        pl_3j.Left = ClientSize.Width / 2 - pl_3j.Width / 2
        pl_4j.Left = (ClientSize.Width / 3) * 2 - pl_4j.Width / 2 + 100
        btn_retour.Left = pl_2j.Left
        btn_validé.Left = pl_4j.Left
    End Sub

    Private Sub Btn_j2_Click(sender As Object, e As EventArgs) Handles btn_j2.Click
        'Si on clique sur le bouton 2 joueurs on rend visible le panel 2 joueurs et on cache les autres panel celui 3 joueurs et 4 joueurs'
        pl_2j.Visible = True
        pl_3j.Visible = False
        pl_4j.Visible = False

        joueur2 = True
        joueur3 = False
        joueur4 = False

        comB_j1_2j.SelectedIndex = 0
        comB_j2_2j.SelectedIndex = 0
        'Si on clique sur le bouton 2 joueurs on efface le contenue des textbox et on déselectionne le premier chiffre des combobox des panel 3 joueurs et 4 joueurs'
        'pour eviter les conflies si les joueurs on entre des pseudos dans deux panel'
        txtbj1_3j.Clear()
        txtbj2_3j.Clear()
        txtbj3_3j.Clear()
        comB_j1_3j.SelectedItem = Nothing
        comB_j2_3j.SelectedItem = Nothing
        comB_j3_3j.SelectedItem = Nothing
        txtbj1_4j.Clear()
        txtbj2_4j.Clear()
        txtbj3_4j.Clear()
        txtbj4_4j.Clear()
        comB_j1_4j.SelectedItem = Nothing
        comB_j2_4j.SelectedItem = Nothing
        comB_j3_4j.SelectedItem = Nothing
        comB_j4_4j.SelectedItem = Nothing
    End Sub

    Private Sub Btn_j3_Click(sender As Object, e As EventArgs) Handles btn_j3.Click
        'Si on clique sur le bouton 3 joueurs on rend visible le panel 3 joueurs et on cache les autres panel celui 2 joueurs et 4 joueurs'
        pl_2j.Visible = False
        pl_3j.Visible = True
        pl_4j.Visible = False
        joueur2 = False
        joueur3 = True
        joueur4 = False
        comB_j1_3j.SelectedIndex = 0
        comB_j2_3j.SelectedIndex = 0
        comB_j3_3j.SelectedIndex = 0
        'Si on clique sur le bouton 3 joueurs on efface le contenue des textbox et on déselectionne le premier chiffre des combobox des panel 2 joueurs et 4 joueurs'
        'pour eviter les conflies si les joueurs on entre des pseudos dans deux panel'
        txtbj1_2j.Clear()
        txtbj2_2j.Clear()
        comB_j1_2j.SelectedItem = Nothing
        comB_j2_2j.SelectedItem = Nothing
        txtbj1_4j.Clear()
        txtbj2_4j.Clear()
        txtbj3_4j.Clear()
        txtbj4_4j.Clear()
        comB_j1_4j.SelectedItem = Nothing
        comB_j2_4j.SelectedItem = Nothing
        comB_j3_4j.SelectedItem = Nothing
        comB_j4_4j.SelectedItem = Nothing
    End Sub

    Private Sub Btn_j4_Click(sender As Object, e As EventArgs) Handles btn_j4.Click
        'Si on clique sur le bouton 4 joueurs on rend visible le panel 4 joueurs et on cache les autres panel celui 2 joueurs et 3 joueurs'
        pl_2j.Visible = False
        pl_3j.Visible = False
        pl_4j.Visible = True
        joueur2 = False
        joueur3 = False
        joueur4 = True
        comB_j1_4j.SelectedIndex = 0
        comB_j2_4j.SelectedIndex = 0
        comB_j3_4j.SelectedIndex = 0
        comB_j4_4j.SelectedIndex = 0
        'Si on clique sur le bouton 4 joueurs on efface le contenue des textbox et on déselectionne le premier chiffre des combobox des panel 2 joueurs et 3 joueurs'
        'pour eviter les conflies si les joueurs on entre des pseudos dans deux panel'
        txtbj1_2j.Clear()
        txtbj2_2j.Clear()
        comB_j1_2j.SelectedItem = Nothing
        comB_j2_2j.SelectedItem = Nothing
        txtbj1_3j.Clear()
        txtbj2_3j.Clear()
        txtbj3_3j.Clear()
        comB_j1_3j.SelectedItem = Nothing
        comB_j2_3j.SelectedItem = Nothing
        comB_j3_3j.SelectedItem = Nothing
    End Sub

    Private Sub Btn_validé_Click(sender As Object, e As EventArgs) Handles btn_validé.Click

        'selon sur quel bouton on a cliqué on regarde si les joueurs on entré des pseudos, si c'est pas le cas on affiche un message sinon'
        'on crée les joueurs avec un pseudo, un age, un score(qui est égal à 0 car la partie n'a pas commencer' 
        If (joueur2 = True) Then
            If (txtbj1_2j.Text = Nothing Or txtbj2_2j.Text = Nothing) Then
                MessageBox.Show("Veuillez saisir des pseudos")
            Else
                If comB_j1_2j.Text > comB_j2_2j.Text Then
                    j1 = New Joueurs(txtbj1_2j.Text, comB_j1_2j.Text, 0)
                    j2 = New Joueurs(txtbj2_2j.Text, comB_j2_2j.Text, 0)
                Else
                    j2 = New Joueurs(txtbj1_2j.Text, comB_j1_2j.Text, 0)
                    j1 = New Joueurs(txtbj2_2j.Text, comB_j2_2j.Text, 0)
                End If
                plateau_vb.Show()
            End If
        End If

        If (joueur3 = True) Then
            If (txtbj1_3j.Text = Nothing Or txtbj2_3j.Text = Nothing Or txtbj3_3j.Text = Nothing) Then
                MessageBox.Show("Veuillez saisir des pseudos")
            Else
                If comB_j1_3j.Text > comB_j2_3j.Text And comB_j1_3j.Text > comB_j3_3j.Text Then
                    j1 = New Joueurs(txtbj1_3j.Text, comB_j1_3j.Text, 0)
                    j2 = New Joueurs(txtbj2_3j.Text, comB_j2_3j.Text, 0)
                    j3 = New Joueurs(txtbj3_3j.Text, comB_j3_3j.Text, 0)
                ElseIf comB_j2_3j.Text > comB_j1_3j.Text And comB_j2_3j.Text > comB_j3_3j.Text Then
                    j3 = New Joueurs(txtbj1_3j.Text, comB_j1_3j.Text, 0)
                    j1 = New Joueurs(txtbj2_3j.Text, comB_j2_3j.Text, 0)
                    j2 = New Joueurs(txtbj3_3j.Text, comB_j3_3j.Text, 0)
                ElseIf comB_j3_3j.Text > comB_j1_3j.Text And comB_j3_3j.Text > comB_j2_3j.Text Then
                    j2 = New Joueurs(txtbj1_3j.Text, comB_j1_3j.Text, 0)
                    j3 = New Joueurs(txtbj2_3j.Text, comB_j2_3j.Text, 0)
                    j1 = New Joueurs(txtbj3_3j.Text, comB_j3_3j.Text, 0)
                End If

                plateau_vb.Show()
            End If

        End If

        If (joueur4 = True) Then
            If (txtbj1_4j.Text = Nothing Or txtbj2_4j.Text = Nothing Or txtbj3_4j.Text = Nothing Or txtbj4_4j.Text = Nothing) Then
                MessageBox.Show("Veuillez saisir des pseudos")
            Else
                If comB_j1_4j.Text > comB_j2_4j.Text And comB_j1_4j.Text > comB_j1_4j.Text And comB_j3_4j.Text > comB_j4_4j.Text Then
                    j1 = New Joueurs(txtbj1_4j.Text, comB_j1_4j.Text, 0)
                    j2 = New Joueurs(txtbj2_4j.Text, comB_j2_4j.Text, 0)
                    j3 = New Joueurs(txtbj3_4j.Text, comB_j3_4j.Text, 0)
                    j4 = New Joueurs(txtbj4_4j.Text, comB_j4_4j.Text, 0)
                ElseIf comB_j2_4j.Text > comB_j1_4j.Text And comB_j2_4j.Text > comB_j3_4j.Text And comB_j2_4j.Text > comB_j4_4j.Text Then
                    j4 = New Joueurs(txtbj1_4j.Text, comB_j1_4j.Text, 0)
                    j1 = New Joueurs(txtbj2_4j.Text, comB_j2_4j.Text, 0)
                    j2 = New Joueurs(txtbj3_4j.Text, comB_j3_4j.Text, 0)
                    j3 = New Joueurs(txtbj4_4j.Text, comB_j4_4j.Text, 0)
                ElseIf comB_j3_4j.Text > comB_j1_4j.Text And comB_j3_4j.Text > comB_j2_4j.Text And comB_j3_4j.Text > comB_j4_4j.Text Then
                    j3 = New Joueurs(txtbj1_4j.Text, comB_j1_4j.Text, 0)
                    j4 = New Joueurs(txtbj2_4j.Text, comB_j2_4j.Text, 0)
                    j1 = New Joueurs(txtbj3_4j.Text, comB_j3_4j.Text, 0)
                    j2 = New Joueurs(txtbj4_4j.Text, comB_j4_4j.Text, 0)
                ElseIf comB_j4_4j.Text > comB_j1_4j.Text And comB_j4_4j.Text > comB_j2_4j.Text And comB_j4_4j.Text > comB_j3_4j.Text Then
                    j2 = New Joueurs(txtbj1_4j.Text, comB_j1_4j.Text, 0)
                    j3 = New Joueurs(txtbj2_4j.Text, comB_j2_4j.Text, 0)
                    j4 = New Joueurs(txtbj3_4j.Text, comB_j3_4j.Text, 0)
                    j1 = New Joueurs(txtbj4_4j.Text, comB_j4_4j.Text, 0)
                End If

                plateau_vb.Show()
            End If
        End If

    End Sub

    Private Sub Btn_retour_Click(sender As Object, e As EventArgs) Handles btn_retour.Click
        Me.Hide()
        accueil.Show()
    End Sub

    'Quand on passe la souris sur les textbox une bulle info apparait'
    Private Sub Txtbj1_2j_MouseHover(sender As Object, e As EventArgs) Handles txtbj1_2j.MouseHover
        ToolTip1.SetToolTip(txtbj1_2j, "Saisissez le nom du joueur 1")
    End Sub

    Private Sub Txtbj2_2j_MouseHover(sender As Object, e As EventArgs) Handles txtbj2_2j.MouseHover
        ToolTip2.SetToolTip(txtbj2_2j, "Saisissez le nom du joueur 2")
    End Sub
    Private Sub Txtbj1_3j_MouseHover(sender As Object, e As EventArgs) Handles txtbj1_3j.MouseHover
        ToolTip3.SetToolTip(txtbj1_3j, "Saisissez le nom du joueur 1")
    End Sub
    Private Sub Txtbj2_3j_MouseHover(sender As Object, e As EventArgs) Handles txtbj2_3j.MouseHover
        ToolTip4.SetToolTip(txtbj2_3j, "Saisissez le nom du joueur 2")
    End Sub
    Private Sub Txtbj3_3j_MouseHover(sender As Object, e As EventArgs) Handles txtbj3_3j.MouseHover
        ToolTip5.SetToolTip(txtbj3_3j, "Saisissez le nom du joueur 3")
    End Sub
    Private Sub Txtbj1_4j_MouseHover(sender As Object, e As EventArgs) Handles txtbj1_4j.MouseHover
        ToolTip6.SetToolTip(txtbj1_4j, "Saisissez le nom du joueur 1")
    End Sub
    Private Sub Txtbj2_4j_MouseHover(sender As Object, e As EventArgs) Handles txtbj2_4j.MouseHover
        ToolTip6.SetToolTip(txtbj2_4j, "Saisissez le nom du joueur 2")
    End Sub
    Private Sub Txtbj3_4j_MouseHover(sender As Object, e As EventArgs) Handles txtbj3_4j.MouseHover
        ToolTip7.SetToolTip(txtbj3_4j, "Saisissez le nom du joueur 3")
    End Sub
    Private Sub Txtbj4_4j_MouseHover(sender As Object, e As EventArgs) Handles txtbj4_4j.MouseHover
        ToolTip8.SetToolTip(txtbj4_4j, "Saisissez le nom du joueur 4")
    End Sub

    Private Sub Nbr_de_joueurs_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        btn_j2.Left = ClientSize.Width / 3 - pl_2j.Width / 2 - 100
        btn_j3.Left = ClientSize.Width / 2 - pl_3j.Width / 2
        btn_j4.Left = (ClientSize.Width / 3) * 2 - pl_4j.Width / 2 + 100
        pl_2j.Left = ClientSize.Width / 3 - pl_2j.Width / 2 - 100
        pl_3j.Left = ClientSize.Width / 2 - pl_3j.Width / 2
        pl_4j.Left = (ClientSize.Width / 3) * 2 - pl_4j.Width / 2 + 100
        btn_retour.Left = pl_2j.Left
        btn_validé.Left = pl_4j.Left
    End Sub
End Class