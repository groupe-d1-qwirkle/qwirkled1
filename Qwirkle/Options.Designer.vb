﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Options
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.sb_volumebruit = New System.Windows.Forms.HScrollBar()
        Me.sb_volumejeu = New System.Windows.Forms.HScrollBar()
        Me.lbl_volumedesbruits = New System.Windows.Forms.Label()
        Me.lbl_volumedujeu = New System.Windows.Forms.Label()
        Me.lbl_background = New System.Windows.Forms.Label()
        Me.lbl_options = New System.Windows.Forms.Label()
        Me.cb_black = New System.Windows.Forms.CheckBox()
        Me.cb_white = New System.Windows.Forms.CheckBox()
        Me.btn_return = New System.Windows.Forms.Button()
        Me.btn_save = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.sb_volumebruit)
        Me.GroupBox1.Controls.Add(Me.sb_volumejeu)
        Me.GroupBox1.Controls.Add(Me.lbl_volumedesbruits)
        Me.GroupBox1.Controls.Add(Me.lbl_volumedujeu)
        Me.GroupBox1.Controls.Add(Me.lbl_background)
        Me.GroupBox1.Controls.Add(Me.lbl_options)
        Me.GroupBox1.Controls.Add(Me.cb_black)
        Me.GroupBox1.Controls.Add(Me.cb_white)
        Me.GroupBox1.Controls.Add(Me.btn_return)
        Me.GroupBox1.Controls.Add(Me.btn_save)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(576, 342)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'sb_volumebruit
        '
        Me.sb_volumebruit.Location = New System.Drawing.Point(232, 223)
        Me.sb_volumebruit.Name = "sb_volumebruit"
        Me.sb_volumebruit.Size = New System.Drawing.Size(310, 21)
        Me.sb_volumebruit.TabIndex = 19
        '
        'sb_volumejeu
        '
        Me.sb_volumejeu.Location = New System.Drawing.Point(232, 161)
        Me.sb_volumejeu.Name = "sb_volumejeu"
        Me.sb_volumejeu.Size = New System.Drawing.Size(310, 21)
        Me.sb_volumejeu.TabIndex = 18
        '
        'lbl_volumedesbruits
        '
        Me.lbl_volumedesbruits.AutoSize = True
        Me.lbl_volumedesbruits.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_volumedesbruits.ForeColor = System.Drawing.Color.IndianRed
        Me.lbl_volumedesbruits.Location = New System.Drawing.Point(19, 223)
        Me.lbl_volumedesbruits.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_volumedesbruits.Name = "lbl_volumedesbruits"
        Me.lbl_volumedesbruits.Size = New System.Drawing.Size(140, 20)
        Me.lbl_volumedesbruits.TabIndex = 17
        Me.lbl_volumedesbruits.Text = "Volume des bruits:"
        '
        'lbl_volumedujeu
        '
        Me.lbl_volumedujeu.AutoSize = True
        Me.lbl_volumedujeu.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_volumedujeu.ForeColor = System.Drawing.Color.IndianRed
        Me.lbl_volumedujeu.Location = New System.Drawing.Point(16, 161)
        Me.lbl_volumedujeu.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_volumedujeu.Name = "lbl_volumedujeu"
        Me.lbl_volumedujeu.Size = New System.Drawing.Size(119, 20)
        Me.lbl_volumedujeu.TabIndex = 16
        Me.lbl_volumedujeu.Text = "Volume du Jeu:"
        '
        'lbl_background
        '
        Me.lbl_background.AutoSize = True
        Me.lbl_background.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_background.ForeColor = System.Drawing.Color.IndianRed
        Me.lbl_background.Location = New System.Drawing.Point(15, 100)
        Me.lbl_background.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_background.Name = "lbl_background"
        Me.lbl_background.Size = New System.Drawing.Size(182, 20)
        Me.lbl_background.TabIndex = 15
        Me.lbl_background.Text = "Couleur du fond d'écran:"
        '
        'lbl_options
        '
        Me.lbl_options.AutoSize = True
        Me.lbl_options.Font = New System.Drawing.Font("Microsoft Sans Serif", 48.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_options.ForeColor = System.Drawing.Color.IndianRed
        Me.lbl_options.Location = New System.Drawing.Point(161, 9)
        Me.lbl_options.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_options.Name = "lbl_options"
        Me.lbl_options.Size = New System.Drawing.Size(254, 73)
        Me.lbl_options.TabIndex = 14
        Me.lbl_options.Text = "Options"
        '
        'cb_black
        '
        Me.cb_black.AutoSize = True
        Me.cb_black.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_black.ForeColor = System.Drawing.Color.IndianRed
        Me.cb_black.Location = New System.Drawing.Point(393, 102)
        Me.cb_black.Margin = New System.Windows.Forms.Padding(2)
        Me.cb_black.Name = "cb_black"
        Me.cb_black.Size = New System.Drawing.Size(169, 21)
        Me.cb_black.TabIndex = 13
        Me.cb_black.Text = "Couleur Noir (Sombre)"
        Me.cb_black.UseVisualStyleBackColor = True
        '
        'cb_white
        '
        Me.cb_white.AutoSize = True
        Me.cb_white.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_white.ForeColor = System.Drawing.Color.IndianRed
        Me.cb_white.Location = New System.Drawing.Point(232, 102)
        Me.cb_white.Margin = New System.Windows.Forms.Padding(2)
        Me.cb_white.Name = "cb_white"
        Me.cb_white.Size = New System.Drawing.Size(156, 21)
        Me.cb_white.TabIndex = 12
        Me.cb_white.Text = "Couleur blanc (Clair)"
        Me.cb_white.UseVisualStyleBackColor = True
        '
        'btn_return
        '
        Me.btn_return.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_return.ForeColor = System.Drawing.Color.IndianRed
        Me.btn_return.Location = New System.Drawing.Point(22, 310)
        Me.btn_return.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_return.Name = "btn_return"
        Me.btn_return.Size = New System.Drawing.Size(98, 24)
        Me.btn_return.TabIndex = 11
        Me.btn_return.Text = "Retour"
        Me.btn_return.UseVisualStyleBackColor = True
        '
        'btn_save
        '
        Me.btn_save.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_save.ForeColor = System.Drawing.Color.IndianRed
        Me.btn_save.Location = New System.Drawing.Point(445, 305)
        Me.btn_save.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_save.Name = "btn_save"
        Me.btn_save.Size = New System.Drawing.Size(98, 24)
        Me.btn_save.TabIndex = 10
        Me.btn_save.Text = "Sauvegarder"
        Me.btn_save.UseVisualStyleBackColor = True
        '
        'Options
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(600, 366)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "Options"
        Me.Text = "Options"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents sb_volumebruit As HScrollBar
    Friend WithEvents sb_volumejeu As HScrollBar
    Friend WithEvents lbl_volumedesbruits As Label
    Friend WithEvents lbl_volumedujeu As Label
    Friend WithEvents lbl_background As Label
    Friend WithEvents lbl_options As Label
    Friend WithEvents cb_black As CheckBox
    Friend WithEvents cb_white As CheckBox
    Friend WithEvents btn_return As Button
    Friend WithEvents btn_save As Button
End Class
