﻿Imports QwirkleClass
Imports Qwirkle.Nbr_de_joueurs


Public Class Score_fin_parti
    Private scorej1, scorej2, scorej3, scorej4 As Integer
    Public largeur As Integer = 70

    Private Sub BtnRejouer_Click(sender As Object, e As EventArgs) Handles BtnRejouer.Click
        Me.Close()
        plateau_vb.Close()
        Nbr_de_joueurs.Close()
        Nbr_de_joueurs.Show()
    End Sub

    Private Sub Score_fin_parti_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If (Nbr_de_joueurs.joueur2 = True) Then
            'on recupére le score du joueur 1 et 2 dans les variables scorej1 et scorej2 les scores plus facilement'
            scorej1 = Nbr_de_joueurs.j1.GetScore
            scorej2 = Nbr_de_joueurs.j2.GetScore
            'on affiche dans des labels le score et le pseudo du joueur 1 et 2'
            lbl_j1.Text = Nbr_de_joueurs.j1.GetPseudo
            lbl_scorej1.Text = Nbr_de_joueurs.j1.GetScore
            lbl_j2.Text = Nbr_de_joueurs.j2.GetPseudo
            lbl_scorej2.Text = Nbr_de_joueurs.j2.GetScore
            'si la parti etait à 2 joueurs on cache le panel du joueur 3 et 4'
            pl_j3_score.Visible = False
            pl_j4_score.Visible = False
            pic_j3.Visible = False
            pic_j4.Visible = False
            'selon le joueur qui a gagné on affiche une coupe en dessous de son pseudo'
            If (scorej1 > scorej2) Then
                pic_j2.Visible = False
            Else
                pic_j1.Visible = False
            End If
        End If

        If (Nbr_de_joueurs.joueur3 = True) Then
            'on recupére le score des joueurs 1,2 et 3 dans les variables scorej1 et scorej2 et scorej3 les scores plus facilement'
            scorej1 = Nbr_de_joueurs.j1.GetScore
            scorej2 = Nbr_de_joueurs.j2.GetScore
            scorej3 = Nbr_de_joueurs.j3.GetScore
            'on affiche dans des labels le score et le pseudo du joueur 1,2 et 3'
            lbl_j1.Text = Nbr_de_joueurs.j1.GetPseudo
            lbl_scorej1.Text = Nbr_de_joueurs.j1.GetScore
            lbl_j2.Text = Nbr_de_joueurs.j2.GetPseudo
            lbl_scorej2.Text = Nbr_de_joueurs.j2.GetScore
            lbl_j3.Text = Nbr_de_joueurs.j3.GetPseudo
            lbl_scorej3.Text = Nbr_de_joueurs.j3.GetScore
            'si la parti etait à 3 joueurs on cache le panel du joueur 4'
            pl_j4_score.Visible = False
            pic_j4.Visible = False
            'selon le joueur qui a gagné on affiche une coupe en dessous de son pseudo'
            If ((scorej1 > scorej2) And (scorej1 > scorej3)) Then
                pic_j2.Visible = False
                pic_j3.Visible = False
            Else
                If ((scorej2 > scorej1) And (scorej2 > scorej3)) Then
                    pic_j1.Visible = False
                    pic_j3.Visible = False
                Else
                    pic_j1.Visible = False
                    pic_j2.Visible = False
                End If
            End If
        End If


        If (Nbr_de_joueurs.joueur4 = True) Then
            'on recupére le score des joueurs 1,2,3 et 4 dans les variables scorej1 et scorej2 et scorej3,scorej4 pour comparer les scores plus facilement'
            scorej1 = Nbr_de_joueurs.j1.GetScore
            scorej2 = Nbr_de_joueurs.j2.GetScore
            scorej3 = Nbr_de_joueurs.j3.GetScore
            scorej4 = Nbr_de_joueurs.j4.GetScore
            'on affiche dans des labels le score et le pseudo du joueur 1,2,3 et 4'
            lbl_j1.Text = Nbr_de_joueurs.j1.GetPseudo
            lbl_scorej1.Text = Nbr_de_joueurs.j1.GetScore
            lbl_j2.Text = Nbr_de_joueurs.j2.GetPseudo
            lbl_scorej2.Text = Nbr_de_joueurs.j2.GetScore
            lbl_j3.Text = Nbr_de_joueurs.j3.GetPseudo
            lbl_scorej3.Text = Nbr_de_joueurs.j3.GetScore
            lbl_j4.Text = Nbr_de_joueurs.j4.GetPseudo
            lbl_scorej4.Text = Nbr_de_joueurs.j4.GetScore
            'selon le joueur qui a gagné on affiche une coupe en dessous de son pseudo'
            If ((scorej1 > scorej2) And (scorej1 > scorej3) And (scorej1 > scorej4)) Then
                pic_j2.Visible = False
                pic_j3.Visible = False
                pic_j4.Visible = False
            Else
                If ((scorej2 > scorej1) And (scorej2 > scorej3) And (scorej2 > scorej4)) Then
                    pic_j1.Visible = False
                    pic_j3.Visible = False
                    pic_j4.Visible = False
                Else
                    If ((scorej3 > scorej1) And (scorej3 > scorej2) And (scorej3 > scorej4)) Then
                        pic_j1.Visible = False
                        pic_j2.Visible = False
                        pic_j4.Visible = False
                    Else
                        pic_j1.Visible = False
                        pic_j2.Visible = False
                        pic_j3.Visible = False
                    End If
                End If
            End If
        End If

        If lbl_j1.Width > 70 And lbl_j1.Width > lbl_j2.Width And lbl_j1.Width > lbl_j3.Width And lbl_j1.Width > lbl_j4.Width Then
            largeur = lbl_j1.Width
        ElseIf lbl_j2.Width > 70 And lbl_j2.Width > lbl_j1.Width And lbl_j2.Width > lbl_j3.Width And lbl_j2.Width > lbl_j4.Width Then
            largeur = lbl_j2.Width
        ElseIf lbl_j3.Width > 70 And lbl_j3.Width > lbl_j1.Width And lbl_j3.Width > lbl_j2.Width And lbl_j3.Width > lbl_j4.Width Then
            largeur = lbl_j3.Width
        ElseIf lbl_j4.Width > 70 And lbl_j4.Width > lbl_j1.Width And lbl_j4.Width > lbl_j2.Width And lbl_j4.Width > lbl_j3.Width Then
            largeur = lbl_j4.Width
        End If

        pl_j1_score.Width = largeur
        pl_j2_score.Width = largeur
        pl_j3_score.Width = largeur
        pl_j4_score.Width = largeur
        BtnQuitter.Width = largeur
        BtnRejouer.Width = largeur

        lbl_j1.Left = pl_j1_score.Width / 2 - lbl_j1.Width / 2
        lbl_j2.Left = pl_j2_score.Width / 2 - lbl_j2.Width / 2
        lbl_j3.Left = pl_j3_score.Width / 2 - lbl_j3.Width / 2
        lbl_j4.Left = pl_j4_score.Width / 2 - lbl_j4.Width / 2

        lbl_scorej1.Left = pl_j1_score.Width / 2 - lbl_scorej1.Width / 2
        lbl_scorej2.Left = pl_j2_score.Width / 2 - lbl_scorej2.Width / 2
        lbl_scorej3.Left = pl_j3_score.Width / 2 - lbl_scorej3.Width / 2
        lbl_scorej4.Left = pl_j4_score.Width / 2 - lbl_scorej4.Width / 2

        pic_j1.Left = pl_j1_score.Width / 2 - pic_j1.Width / 2
        pic_j2.Left = pl_j2_score.Width / 2 - pic_j2.Width / 2
        pic_j3.Left = pl_j3_score.Width / 2 - pic_j3.Width / 2
        pic_j4.Left = pl_j4_score.Width / 2 - pic_j4.Width / 2

        pl_j1_score.Left = ClientSize.Width / 2 - 160 - 2 * largeur
        pl_j2_score.Left = ClientSize.Width / 2 - 60 - largeur
        pl_j3_score.Left = ClientSize.Width / 2 + 60
        pl_j4_score.Left = ClientSize.Width / 2 + 160 + largeur

        BtnQuitter.Left = pl_j4_score.Left
        BtnRejouer.Left = pl_j1_score.Left

    End Sub

    Private Sub BtnQuitter_Click(sender As Object, e As EventArgs) Handles BtnQuitter.Click
        Me.Close()
        accueil.Close()
    End Sub

    Private Sub Score_fin_parti_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        lbl_j1.Left = pl_j1_score.Width / 2 - lbl_j1.Width / 2
        lbl_j2.Left = pl_j2_score.Width / 2 - lbl_j2.Width / 2
        lbl_j3.Left = pl_j3_score.Width / 2 - lbl_j3.Width / 2
        lbl_j4.Left = pl_j4_score.Width / 2 - lbl_j4.Width / 2

        lbl_scorej1.Left = pl_j1_score.Width / 2 - lbl_scorej1.Width / 2
        lbl_scorej2.Left = pl_j2_score.Width / 2 - lbl_scorej2.Width / 2
        lbl_scorej3.Left = pl_j3_score.Width / 2 - lbl_scorej3.Width / 2
        lbl_scorej4.Left = pl_j4_score.Width / 2 - lbl_scorej4.Width / 2

        pic_j1.Left = pl_j1_score.Width / 2 - pic_j1.Width / 2
        pic_j2.Left = pl_j2_score.Width / 2 - pic_j2.Width / 2
        pic_j3.Left = pl_j3_score.Width / 2 - pic_j3.Width / 2
        pic_j4.Left = pl_j4_score.Width / 2 - pic_j4.Width / 2

        pl_j1_score.Left = ClientSize.Width / 2 - 160 - 2 * largeur
        pl_j2_score.Left = ClientSize.Width / 2 - 60 - largeur
        pl_j3_score.Left = ClientSize.Width / 2 + 60
        pl_j4_score.Left = ClientSize.Width / 2 + 160 + largeur

        BtnQuitter.Left = pl_j4_score.Left
        BtnRejouer.Left = pl_j1_score.Left
    End Sub
End Class