﻿Imports QwirkleClass
Imports Qwirkle.Nbr_de_joueurs

Public Class plateau_vb

    Public plateau As Plateau = New Plateau
    Public mainJoueur As List(Of Tuiles)
    Public sacoche As Sac = New Sac
    Public nb_joueur As Integer
    Public joueurActuel As Integer = 1
    Public TuileDragDrop As Tuiles
    Public IndexMain As Integer
    Public premier_tour As Boolean = True
    Public Swap As Boolean = False
    Public compteSwap As Integer = 6

    Private Sub Plateau_vb_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Nbr_de_joueurs.Hide()
        RazMouseMove()
        JoueurNombre()

        If nb_joueur = 2 Then
            LblScoreJ1.Text = String.Format("Score de {0}", Nbr_de_joueurs.j1.GetPseudo)
            LblScoreJ2.Text = String.Format("Score de {0}", Nbr_de_joueurs.j2.GetPseudo)
        ElseIf nb_joueur = 3 Then
            LblScoreJ1.Text = String.Format("Score de {0}", Nbr_de_joueurs.j1.GetPseudo)
            LblScoreJ2.Text = String.Format("Score de {0}", Nbr_de_joueurs.j2.GetPseudo)
            LblScoreJ3.Text = String.Format("Score de {0}", Nbr_de_joueurs.j3.GetPseudo)
        Else
            LblScoreJ1.Text = String.Format("Score de {0}", Nbr_de_joueurs.j1.GetPseudo)
            LblScoreJ2.Text = String.Format("Score de {0}", Nbr_de_joueurs.j2.GetPseudo)
            LblScoreJ3.Text = String.Format("Score de {0}", Nbr_de_joueurs.j3.GetPseudo)
            LblScoreJ4.Text = String.Format("Score de {0}", Nbr_de_joueurs.j4.GetPseudo)
        End If

        Dim LargeurScore As Integer = ClientSize.Width / 4
        Dim LargeurFenetre As Integer = ClientSize.Width
        Dim HauteurGBPion As Integer = ClientSize.Height - GroupBox6.Height

        Me.GroupBox1.Size = New Size(LargeurScore, 28)
        Me.GroupBox2.Size = New Size(LargeurScore, 28)
        Me.GroupBox3.Size = New Size(LargeurScore, 28)
        Me.GroupBox4.Size = New Size(LargeurScore, 28)
        Me.GroupBox5.Size = New Size(LargeurFenetre, 29)
        Me.GroupBox6.Size = New Size(LargeurFenetre, 29)
        Me.GroupBox1.Left = 0
        Me.GroupBox2.Left = LargeurScore
        Me.GroupBox3.Left = 2 * LargeurScore
        Me.GroupBox4.Left = 3 * LargeurScore
        Me.GroupBox6.Left = 0
        Me.GroupBox1.Top = 0
        Me.GroupBox2.Top = 0
        Me.GroupBox3.Top = 0
        Me.GroupBox4.Top = 0
        Me.GroupBox6.Top = HauteurGBPion

    End Sub

    Private Sub InitialisationPlateau()

        SuspendLayout()

        For colonne = 0 To plateau.LargeurPlateau() - 1

            For ligne = 0 To plateau.HauteurPlateau() - 1

                Dim tuilePlateau As Tuiles = plateau.GetPlateau(colonne, ligne)
                Dim couleur As Integer = tuilePlateau.Couleur()
                Dim forme As Integer = tuilePlateau.Forme()
                Dim ptcBox As PictureBox = New PictureBox With
                {
                    .Name = String.Format("ptcBox_{0}_{1}", colonne, ligne),
                    .Size = New Size(40, 40),
                    .Image = Image.FromFile(String.Format("../../Resources/{0}{1}.jpg", couleur, forme)),
                    .SizeMode = PictureBoxSizeMode.StretchImage,
                    .Location = New Point((20 + (colonne * 40)), (20 + (ligne * 40))),
                    .AllowDrop = True
                }

                If Swap = True Then
                    RemoveHandler ptcBox.DragDrop, AddressOf PlateauDragDrop
                    RemoveHandler ptcBox.DragEnter, AddressOf PlateauDragEnter
                Else
                    AddHandler ptcBox.DragDrop, AddressOf PlateauDragDrop
                    AddHandler ptcBox.DragEnter, AddressOf PlateauDragEnter
                End If

                Me.pnl_plateau.Controls.Add(ptcBox)

            Next

        Next

        Dim largeur As Integer = ((plateau.LargeurPlateau() * 40) + 20)
        Dim hauteur As Integer = ((plateau.HauteurPlateau() * 40) + 20)
        Dim PositionGauche As Integer = ClientSize.Width / 2 - ((plateau.LargeurPlateau() * 40) + 20) / 2
        Dim PositionHaut As Integer = (ClientSize.Height / 2 - ((plateau.HauteurPlateau() * 40) + 20) / 2) - 20

        If pnl_plateau.Width < ((ClientSize.Width / 100) * 95) Then

            pnl_plateau.Width = largeur
            pnl_plateau.Left = PositionGauche

        End If

        If pnl_plateau.Height < ((ClientSize.Height / 100) * 70) Then

            pnl_plateau.Height = hauteur
            pnl_plateau.Top = PositionHaut

        End If

        ResumeLayout()

    End Sub

    Private Sub Mouse_Move(sender As Object, e As MouseEventArgs)

        Dim effectRealise As DragDropEffects
        Dim ptc_drag As PictureBox = sender

        If e.Button = MouseButtons.Left Then
            If ptc_drag.Image IsNot Nothing Then
                effectRealise = ptc_drag.DoDragDrop(ptc_drag, DragDropEffects.Move)
            End If
        End If

    End Sub

    Private Sub PlateauDragEnter(sender As Object, e As DragEventArgs) Handles PictureBox_99_99.DragEnter

        Dim ptc_DragEnter As PictureBox = sender

        If ptc_DragEnter.Name = "PictureBox_99_99" Then
            Swap = True
        End If

        If e.Data.GetDataPresent("System.Windows.Forms.PictureBox") Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If

    End Sub

    Private Sub PlateauDragDrop(sender As Object, e As DragEventArgs) Handles PictureBox_99_99.DragDrop

        Dim ptc_DragDrop As PictureBox = sender

        Dim x As Short = Short.Parse(ptc_DragDrop.Name.Split("_")(1))
        Dim y As Short = Short.Parse(ptc_DragDrop.Name.Split("_")(2))
        Dim test As Boolean = False

        ptc_DragDrop = e.Data.GetData("System.Windows.Forms.PictureBox")
        IndexMainChoisis(ptc_DragDrop)
        TuileChoisisIndex(IndexMain)

        If Swap = True Then

            pnl_plateau.Controls.Clear()
            SupressionMainJoueur(IndexMain)

            If joueurActuel = 1 Then

                Nbr_de_joueurs.j1.TuilesManquantes(sacoche)

            ElseIf joueurActuel = 2 Then

                Nbr_de_joueurs.j2.TuilesManquantes(sacoche)

            ElseIf joueurActuel = 3 Then

                Nbr_de_joueurs.j3.TuilesManquantes(sacoche)

            ElseIf joueurActuel Like 4 Then

                Nbr_de_joueurs.j4.TuilesManquantes(sacoche)

            End If
            RechargementVisuelMain()
            compteSwap = compteSwap - 1
            RazMouseMove()
            InitialisationPlateau()

        Else

            test = plateau.AddTuile(x, y, TuileDragDrop)

        End If


        If test = True Then

            PictureBox_99_99.AllowDrop = False
            pnl_plateau.Controls.Clear()
            SupressionMainJoueur(IndexMain)
            RechargementVisuelMain()
            InitialisationPlateau()

        ElseIf Swap = True Then
            MsgBox("Tuile échangé")
        End If


    End Sub

    Private Sub Btn_start_Click(sender As Object, e As EventArgs) Handles btn_start.Click

        btn_start.Visible = False
        btn_valideTour.Visible = True
        PictureBox_99_99.Visible = True
        PictureBox_99_99.AllowDrop = True
        btn_stop.Visible = True

        LblPionJoueur.Text = String.Format("Pion de {0}", Nbr_de_joueurs.j1.GetPseudo)

        InitialisationPlateau()

    End Sub

    Private Sub JoueurNombre()

        nb_joueur = 4

        If (Nbr_de_joueurs.joueur2 = True) Then
            GroupBox3.Visible = False
            GroupBox4.Visible = False
            nb_joueur = 2
        ElseIf Nbr_de_joueurs.joueur3 = True Then
            GroupBox4.Visible = False
            nb_joueur = 3
        End If

    End Sub

    Private Sub PremierTour(sender As Object, e As EventArgs) Handles btn_start.Click

        SuspendLayout()

        If joueurActuel Like 1 Then
            Nbr_de_joueurs.j1.TuilesManquantes(sacoche)
            For i = 0 To 5

                Dim PB As PictureBox = Me.GB_Pion_joueur.Controls.Item(String.Format("PB{0}", i))
                Dim tuile As Tuiles = Nbr_de_joueurs.j1.GetMainIndex(i)
                PB.Image = Image.FromFile(String.Format("..\..\Resources\{0}{1}.jpg", tuile.Couleur, tuile.Forme))

            Next
        End If

        ResumeLayout()

    End Sub

    Private Sub Btn_valideTour_Click(sender As Object, e As EventArgs) Handles btn_valideTour.Click
        FinDuJeu()
        Swap = False
        pnl_plateau.Controls.Clear()
        plateau.ResetBool()
        InitialisationPlateau()
        RazMouseMove()

        SuspendLayout()

        PictureBox_99_99.AllowDrop = True
        compteSwap = 6

        CalculScore(joueurActuel)

        If nb_joueur Like 2 And joueurActuel Like 2 Then
            joueurActuel = 0
        ElseIf nb_joueur Like 3 And joueurActuel Like 3 Then
            joueurActuel = 0
        ElseIf nb_joueur Like 4 And joueurActuel Like 4 Then
            joueurActuel = 0
        End If

        joueurActuel += 1

        If joueurActuel Like 1 Then

            LblPionJoueur.Text = String.Format("Pion de {0}", Nbr_de_joueurs.j1.GetPseudo)

            Nbr_de_joueurs.j1.TuilesManquantes(sacoche)

        ElseIf joueurActuel Like 2 Then

            LblPionJoueur.Text = String.Format("Pion de {0}", Nbr_de_joueurs.j2.GetPseudo)

            Nbr_de_joueurs.j2.TuilesManquantes(sacoche)

        ElseIf joueurActuel Like 3 Then

            LblPionJoueur.Text = String.Format("Pion de {0}", Nbr_de_joueurs.j3.GetPseudo)

            Nbr_de_joueurs.j3.TuilesManquantes(sacoche)

        ElseIf joueurActuel Like 4 Then

            LblPionJoueur.Text = String.Format("Pion de {0}", Nbr_de_joueurs.j4.GetPseudo)

            Nbr_de_joueurs.j4.TuilesManquantes(sacoche)

        End If

        RechargementVisuelMain()

        ResumeLayout()

    End Sub

    Private Sub IndexMainChoisis(ptc_DragDrop)

        Dim pic As PictureBox = ptc_DragDrop

        If pic.Name = "PB0" Then
            IndexMain = 0
        ElseIf pic.Name = "PB1" Then
            IndexMain = 1
        ElseIf pic.Name = "PB2" Then
            IndexMain = 2
        ElseIf pic.Name = "PB3" Then
            IndexMain = 3
        ElseIf pic.Name = "PB4" Then
            IndexMain = 4
        ElseIf pic.Name = "PB5" Then
            IndexMain = 5
        End If

    End Sub

    Private Sub SupressionMainJoueur(index)

        If joueurActuel = 1 Then
            Nbr_de_joueurs.j1.DeletItemmain(index)
        ElseIf joueurActuel = 2 Then
            Nbr_de_joueurs.j2.DeletItemmain(index)
        ElseIf joueurActuel = 3 Then
            Nbr_de_joueurs.j3.DeletItemmain(index)
        ElseIf joueurActuel = 4 Then
            Nbr_de_joueurs.j4.DeletItemmain(index)
        End If

    End Sub

    Private Sub TuileChoisisIndex(index)

        If joueurActuel = 1 Then
            TuileDragDrop = Nbr_de_joueurs.j1.GetMainIndex(index)
        ElseIf joueurActuel = 2 Then
            TuileDragDrop = Nbr_de_joueurs.j2.GetMainIndex(index)
        ElseIf joueurActuel = 3 Then
            TuileDragDrop = Nbr_de_joueurs.j3.GetMainIndex(index)
        ElseIf joueurActuel = 4 Then
            TuileDragDrop = Nbr_de_joueurs.j4.GetMainIndex(index)
        End If

    End Sub

    Private Sub RechargementVisuelMain()

        SuspendLayout()

        Dim Nombre As Integer
        Dim Iteration As Integer

        If joueurActuel = 1 Then
            Nombre = Nbr_de_joueurs.j1.CompteurMain()
            For i = 0 To Nombre - 1

                Dim PB As PictureBox = Me.GB_Pion_joueur.Controls.Item(String.Format("PB{0}", i))
                Dim tuile As Tuiles = Nbr_de_joueurs.j1.GetMainIndex(i)
                PB.Image = Image.FromFile(String.Format("..\..\Resources\{0}{1}.jpg", tuile.Couleur, tuile.Forme))

            Next

            For Iteration = Nombre To 5

                Dim PB As PictureBox = Me.GB_Pion_joueur.Controls.Item(String.Format("PB{0}", Iteration))
                PB.Image = Nothing

            Next

        ElseIf joueurActuel = 2 Then
            Nombre = Nbr_de_joueurs.j2.CompteurMain()
            For i = 0 To Nombre - 1

                Dim PB As PictureBox = Me.GB_Pion_joueur.Controls.Item(String.Format("PB{0}", i))
                Dim tuile As Tuiles = Nbr_de_joueurs.j2.GetMainIndex(i)
                PB.Image = Image.FromFile(String.Format("..\..\Resources\{0}{1}.jpg", tuile.Couleur, tuile.Forme))

            Next

            For Iteration = Nombre To 5

                Dim PB As PictureBox = Me.GB_Pion_joueur.Controls.Item(String.Format("PB{0}", Iteration))
                PB.Image = Nothing

            Next

        ElseIf joueurActuel = 3 Then

            Nombre = Nbr_de_joueurs.j3.CompteurMain()

            For i = 0 To Nombre - 1

                Dim PB As PictureBox = Me.GB_Pion_joueur.Controls.Item(String.Format("PB{0}", i))
                Dim tuile As Tuiles = Nbr_de_joueurs.j3.GetMainIndex(i)
                PB.Image = Image.FromFile(String.Format("..\..\Resources\{0}{1}.jpg", tuile.Couleur, tuile.Forme))

            Next

            For Iteration = Nombre To 5

                Dim PB As PictureBox = Me.GB_Pion_joueur.Controls.Item(String.Format("PB{0}", Iteration))
                PB.Image = Nothing

            Next

        ElseIf joueurActuel = 4 Then

            Nombre = Nbr_de_joueurs.j4.CompteurMain()

            For i = 0 To Nombre - 1

                Dim PB As PictureBox = Me.GB_Pion_joueur.Controls.Item(String.Format("PB{0}", i))
                Dim tuile As Tuiles = Nbr_de_joueurs.j4.GetMainIndex(i)
                PB.Image = Image.FromFile(String.Format("..\..\Resources\{0}{1}.jpg", tuile.Couleur, tuile.Forme))

            Next

            For Iteration = Nombre To 5

                Dim PB As PictureBox = Me.GB_Pion_joueur.Controls.Item(String.Format("PB{0}", Iteration))
                PB.Image = Nothing

            Next

        End If

        ResumeLayout()

    End Sub

    Private Sub CalculScore(JoueurActuel)

        Dim Score As Integer = 0

        Score = plateau.ScoreCalcul()
        Score = Score + plateau.ScoreQwirkle()

        If JoueurActuel = 1 Then
            Nbr_de_joueurs.j1.IncrementationScore(Score)
            Label2.Text = String.Format("{0}", Nbr_de_joueurs.j1.GetScore)
        ElseIf JoueurActuel = 2 Then
            Nbr_de_joueurs.j2.IncrementationScore(Score)
            Label5.Text = String.Format("{0}", Nbr_de_joueurs.j2.GetScore)
        ElseIf JoueurActuel = 3 Then
            Nbr_de_joueurs.j3.IncrementationScore(Score)
            Label8.Text = String.Format("{0}", Nbr_de_joueurs.j3.GetScore)
        ElseIf JoueurActuel = 4 Then
            Nbr_de_joueurs.j4.IncrementationScore(Score)
            Label11.Text = String.Format("{0}", Nbr_de_joueurs.j4.GetScore)
        End If

        plateau.ArchiveTuile()

    End Sub

    Private Sub RazMouseMove()

        If Swap = True Then
            For iteration = compteSwap To 5
                Dim pic As PictureBox = Me.GB_Pion_joueur.Controls.Item(String.Format("PB{0}", iteration))
                RemoveHandler pic.MouseMove, AddressOf Mouse_Move
            Next
        Else
            For iteration = 0 To 5
                Dim pic As PictureBox = Me.GB_Pion_joueur.Controls.Item(String.Format("PB{0}", iteration))
                AddHandler pic.MouseMove, AddressOf Mouse_Move
            Next
        End If

    End Sub

    Private Sub Plateau_vb_Resize(sender As Object, e As EventArgs) Handles Me.Resize

        Dim largeur As Integer = ((plateau.LargeurPlateau() * 40) + 20)
        Dim hauteur As Integer = ((plateau.HauteurPlateau() * 40) + 20)
        Dim PositionGauche As Integer = ClientSize.Width / 2 - ((plateau.LargeurPlateau() * 40) + 20) / 2
        Dim PositionHaut As Integer = ClientSize.Height / 2 - ((plateau.HauteurPlateau() * 40) + 20) / 2

        If pnl_plateau.Width < ((ClientSize.Width / 100) * 95) Then

            pnl_plateau.Width = largeur
            pnl_plateau.Left = PositionGauche

        End If

        If pnl_plateau.Height < ((ClientSize.Height / 100) * 70) Then

            pnl_plateau.Height = hauteur
            pnl_plateau.Top = PositionHaut

        End If

        Dim LargeurScore As Integer = ClientSize.Width / 4
        Dim LargeurFenetre As Integer = ClientSize.Width
        Dim HauteurGBPion As Integer = ClientSize.Height - GroupBox6.Height

        Me.GroupBox1.Size = New Size(LargeurScore, 28)
        Me.GroupBox2.Size = New Size(LargeurScore, 28)
        Me.GroupBox3.Size = New Size(LargeurScore, 28)
        Me.GroupBox4.Size = New Size(LargeurScore, 28)
        Me.GroupBox5.Size = New Size(LargeurFenetre, 29)
        Me.GroupBox6.Size = New Size(LargeurFenetre, 29)
        Me.GroupBox1.Left = 0
        Me.GroupBox2.Left = LargeurScore
        Me.GroupBox3.Left = 2 * LargeurScore
        Me.GroupBox4.Left = 3 * LargeurScore
        Me.GroupBox6.Left = 0
        Me.GroupBox1.Top = 0
        Me.GroupBox2.Top = 0
        Me.GroupBox3.Top = 0
        Me.GroupBox4.Top = 0
        Me.GroupBox6.Top = HauteurGBPion
        Me.GroupBox7.Left = GroupBox6.Width - GroupBox7.Width

    End Sub

    Private Sub BtnMenu_Click(sender As Object, e As EventArgs) Handles BtnMenu.Click

        accueil.Show()
        Me.Close()
        Nbr_de_joueurs.Close()

    End Sub

    Private Sub Btn_stop_Click(sender As Object, e As EventArgs) Handles btn_stop.Click
        Me.Close()
        Score_fin_parti.Show()
        Nbr_de_joueurs.Close()
    End Sub
    Private Sub FinDuJeu()
        If sacoche.IsNotEmpty = False Then
            Score_fin_parti.Show()
            Nbr_de_joueurs.Close()
            Me.Close()
        End If
    End Sub
End Class