﻿Public Class Options

    Private Sub Cb_white_CheckedChanged(sender As Object, e As EventArgs) Handles cb_white.CheckedChanged

        If (cb_white.Checked = True) Then
            cb_black.Checked = False
            BackColor = Color.White
        End If
    End Sub

    Private Sub Cb_black_CheckedChanged(sender As Object, e As EventArgs) Handles cb_black.CheckedChanged
        If (cb_black.Checked = True) Then
            cb_white.Checked = False
            BackColor = Color.Black
        End If
    End Sub

    Private Sub btn_return_Click(sender As Object, e As EventArgs) Handles btn_return.Click
        Me.Close()
        accueil.Show()
    End Sub

    Private Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        accueil.Show()
        Me.Close()
        accueil.BackColor = BackColor
        Regle.BackColor = BackColor
        Nbr_de_joueurs.BackColor = BackColor
        plateau_vb.BackColor = BackColor
        Score_fin_parti.BackColor = BackColor

    End Sub

    Private Sub Options_Load(sender As Object, e As EventArgs) Handles Me.Load
        GroupBox1.Left = ClientSize.Width / 2 - GroupBox1.Width / 2
    End Sub

    Private Sub Options_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        GroupBox1.Left = ClientSize.Width / 2 - GroupBox1.Width / 2
    End Sub
End Class