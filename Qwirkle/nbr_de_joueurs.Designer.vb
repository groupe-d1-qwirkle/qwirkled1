﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Nbr_de_joueurs
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btn_j2 = New System.Windows.Forms.Button()
        Me.btn_j3 = New System.Windows.Forms.Button()
        Me.btn_j4 = New System.Windows.Forms.Button()
        Me.btn_validé = New System.Windows.Forms.Button()
        Me.btn_retour = New System.Windows.Forms.Button()
        Me.pl_2j = New System.Windows.Forms.Panel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.comB_j2_2j = New System.Windows.Forms.ComboBox()
        Me.comB_j1_2j = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtbj2_2j = New System.Windows.Forms.TextBox()
        Me.txtbj1_2j = New System.Windows.Forms.TextBox()
        Me.pl_4j = New System.Windows.Forms.Panel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.comB_j2_4j = New System.Windows.Forms.ComboBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.comB_j4_4j = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.comB_j1_4j = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.comB_j3_4j = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtbj3_4j = New System.Windows.Forms.TextBox()
        Me.txtbj4_4j = New System.Windows.Forms.TextBox()
        Me.txtbj1_4j = New System.Windows.Forms.TextBox()
        Me.txtbj2_4j = New System.Windows.Forms.TextBox()
        Me.pl_3j = New System.Windows.Forms.Panel()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.comB_j3_3j = New System.Windows.Forms.ComboBox()
        Me.comB_j1_3j = New System.Windows.Forms.ComboBox()
        Me.comB_j2_3j = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtbj3_3j = New System.Windows.Forms.TextBox()
        Me.txtbj2_3j = New System.Windows.Forms.TextBox()
        Me.txtbj1_3j = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ToolTip2 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ToolTip3 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ToolTip4 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ToolTip5 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ToolTip6 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ToolTip7 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ToolTip8 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ToolTip9 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ToolTip10 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pl_2j.SuspendLayout()
        Me.pl_4j.SuspendLayout()
        Me.pl_3j.SuspendLayout()
        Me.SuspendLayout()
        '
        'btn_j2
        '
        Me.btn_j2.Location = New System.Drawing.Point(30, 43)
        Me.btn_j2.Name = "btn_j2"
        Me.btn_j2.Size = New System.Drawing.Size(164, 36)
        Me.btn_j2.TabIndex = 0
        Me.btn_j2.Text = "2 Joueurs"
        Me.btn_j2.UseVisualStyleBackColor = True
        '
        'btn_j3
        '
        Me.btn_j3.Location = New System.Drawing.Point(284, 43)
        Me.btn_j3.Name = "btn_j3"
        Me.btn_j3.Size = New System.Drawing.Size(164, 36)
        Me.btn_j3.TabIndex = 1
        Me.btn_j3.Text = "3 Joueurs"
        Me.btn_j3.UseVisualStyleBackColor = True
        '
        'btn_j4
        '
        Me.btn_j4.Location = New System.Drawing.Point(574, 43)
        Me.btn_j4.Name = "btn_j4"
        Me.btn_j4.Size = New System.Drawing.Size(164, 36)
        Me.btn_j4.TabIndex = 2
        Me.btn_j4.Text = "4 Joueurs"
        Me.btn_j4.UseVisualStyleBackColor = True
        '
        'btn_validé
        '
        Me.btn_validé.Location = New System.Drawing.Point(574, 404)
        Me.btn_validé.Name = "btn_validé"
        Me.btn_validé.Size = New System.Drawing.Size(164, 36)
        Me.btn_validé.TabIndex = 3
        Me.btn_validé.Text = "Valider"
        Me.btn_validé.UseVisualStyleBackColor = True
        '
        'btn_retour
        '
        Me.btn_retour.Location = New System.Drawing.Point(30, 404)
        Me.btn_retour.Name = "btn_retour"
        Me.btn_retour.Size = New System.Drawing.Size(164, 36)
        Me.btn_retour.TabIndex = 4
        Me.btn_retour.Text = "Retour"
        Me.btn_retour.UseVisualStyleBackColor = True
        '
        'pl_2j
        '
        Me.pl_2j.Controls.Add(Me.Label11)
        Me.pl_2j.Controls.Add(Me.Label10)
        Me.pl_2j.Controls.Add(Me.comB_j2_2j)
        Me.pl_2j.Controls.Add(Me.comB_j1_2j)
        Me.pl_2j.Controls.Add(Me.Label2)
        Me.pl_2j.Controls.Add(Me.Label1)
        Me.pl_2j.Controls.Add(Me.txtbj2_2j)
        Me.pl_2j.Controls.Add(Me.txtbj1_2j)
        Me.pl_2j.Location = New System.Drawing.Point(30, 123)
        Me.pl_2j.Name = "pl_2j"
        Me.pl_2j.Size = New System.Drawing.Size(164, 251)
        Me.pl_2j.TabIndex = 5
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(7, 93)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(32, 13)
        Me.Label11.TabIndex = 7
        Me.Label11.Text = "Age :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(7, 41)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(35, 13)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Age : "
        '
        'comB_j2_2j
        '
        Me.comB_j2_2j.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comB_j2_2j.FormattingEnabled = True
        Me.comB_j2_2j.Location = New System.Drawing.Point(64, 90)
        Me.comB_j2_2j.Name = "comB_j2_2j"
        Me.comB_j2_2j.Size = New System.Drawing.Size(37, 21)
        Me.comB_j2_2j.TabIndex = 5
        '
        'comB_j1_2j
        '
        Me.comB_j1_2j.AccessibleDescription = "1"
        Me.comB_j1_2j.Cursor = System.Windows.Forms.Cursors.Default
        Me.comB_j1_2j.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comB_j1_2j.FormattingEnabled = True
        Me.comB_j1_2j.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.comB_j1_2j.Location = New System.Drawing.Point(64, 38)
        Me.comB_j1_2j.Name = "comB_j1_2j"
        Me.comB_j1_2j.Size = New System.Drawing.Size(37, 21)
        Me.comB_j1_2j.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 67)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Joueur 2 :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Joueur 1 :"
        '
        'txtbj2_2j
        '
        Me.txtbj2_2j.Location = New System.Drawing.Point(63, 64)
        Me.txtbj2_2j.Name = "txtbj2_2j"
        Me.txtbj2_2j.Size = New System.Drawing.Size(100, 20)
        Me.txtbj2_2j.TabIndex = 1
        '
        'txtbj1_2j
        '
        Me.txtbj1_2j.AllowDrop = True
        Me.txtbj1_2j.Location = New System.Drawing.Point(64, 14)
        Me.txtbj1_2j.Name = "txtbj1_2j"
        Me.txtbj1_2j.Size = New System.Drawing.Size(100, 20)
        Me.txtbj1_2j.TabIndex = 0
        '
        'pl_4j
        '
        Me.pl_4j.Controls.Add(Me.Label15)
        Me.pl_4j.Controls.Add(Me.comB_j2_4j)
        Me.pl_4j.Controls.Add(Me.Label19)
        Me.pl_4j.Controls.Add(Me.comB_j4_4j)
        Me.pl_4j.Controls.Add(Me.Label18)
        Me.pl_4j.Controls.Add(Me.comB_j1_4j)
        Me.pl_4j.Controls.Add(Me.Label8)
        Me.pl_4j.Controls.Add(Me.Label17)
        Me.pl_4j.Controls.Add(Me.comB_j3_4j)
        Me.pl_4j.Controls.Add(Me.Label7)
        Me.pl_4j.Controls.Add(Me.Label6)
        Me.pl_4j.Controls.Add(Me.Label9)
        Me.pl_4j.Controls.Add(Me.txtbj3_4j)
        Me.pl_4j.Controls.Add(Me.txtbj4_4j)
        Me.pl_4j.Controls.Add(Me.txtbj1_4j)
        Me.pl_4j.Controls.Add(Me.txtbj2_4j)
        Me.pl_4j.Location = New System.Drawing.Point(574, 123)
        Me.pl_4j.Name = "pl_4j"
        Me.pl_4j.Size = New System.Drawing.Size(164, 251)
        Me.pl_4j.TabIndex = 6
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(3, 222)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(32, 13)
        Me.Label15.TabIndex = 15
        Me.Label15.Text = "Age :"
        '
        'comB_j2_4j
        '
        Me.comB_j2_4j.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comB_j2_4j.FormattingEnabled = True
        Me.comB_j2_4j.Location = New System.Drawing.Point(63, 100)
        Me.comB_j2_4j.Name = "comB_j2_4j"
        Me.comB_j2_4j.Size = New System.Drawing.Size(37, 21)
        Me.comB_j2_4j.TabIndex = 10
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(3, 164)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(32, 13)
        Me.Label19.TabIndex = 18
        Me.Label19.Text = "Age :"
        '
        'comB_j4_4j
        '
        Me.comB_j4_4j.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comB_j4_4j.FormattingEnabled = True
        Me.comB_j4_4j.Location = New System.Drawing.Point(63, 219)
        Me.comB_j4_4j.Name = "comB_j4_4j"
        Me.comB_j4_4j.Size = New System.Drawing.Size(37, 21)
        Me.comB_j4_4j.TabIndex = 9
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(3, 108)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(32, 13)
        Me.Label18.TabIndex = 17
        Me.Label18.Text = "Age :"
        '
        'comB_j1_4j
        '
        Me.comB_j1_4j.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comB_j1_4j.FormattingEnabled = True
        Me.comB_j1_4j.Location = New System.Drawing.Point(63, 40)
        Me.comB_j1_4j.Name = "comB_j1_4j"
        Me.comB_j1_4j.Size = New System.Drawing.Size(37, 21)
        Me.comB_j1_4j.TabIndex = 11
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(3, 136)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(54, 13)
        Me.Label8.TabIndex = 6
        Me.Label8.Text = "Joueur 3 :"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(3, 43)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(32, 13)
        Me.Label17.TabIndex = 16
        Me.Label17.Text = "Age :"
        '
        'comB_j3_4j
        '
        Me.comB_j3_4j.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comB_j3_4j.FormattingEnabled = True
        Me.comB_j3_4j.Location = New System.Drawing.Point(63, 162)
        Me.comB_j3_4j.Name = "comB_j3_4j"
        Me.comB_j3_4j.Size = New System.Drawing.Size(37, 21)
        Me.comB_j3_4j.TabIndex = 8
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(3, 194)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 13)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "Joueur 4 :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 76)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(54, 13)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Joueur 2 :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(3, 14)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 13)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "Joueur 1 :"
        '
        'txtbj3_4j
        '
        Me.txtbj3_4j.Location = New System.Drawing.Point(63, 133)
        Me.txtbj3_4j.Name = "txtbj3_4j"
        Me.txtbj3_4j.Size = New System.Drawing.Size(100, 20)
        Me.txtbj3_4j.TabIndex = 0
        '
        'txtbj4_4j
        '
        Me.txtbj4_4j.Location = New System.Drawing.Point(63, 189)
        Me.txtbj4_4j.Name = "txtbj4_4j"
        Me.txtbj4_4j.Size = New System.Drawing.Size(100, 20)
        Me.txtbj4_4j.TabIndex = 3
        '
        'txtbj1_4j
        '
        Me.txtbj1_4j.Location = New System.Drawing.Point(63, 10)
        Me.txtbj1_4j.Name = "txtbj1_4j"
        Me.txtbj1_4j.Size = New System.Drawing.Size(100, 20)
        Me.txtbj1_4j.TabIndex = 2
        '
        'txtbj2_4j
        '
        Me.txtbj2_4j.Location = New System.Drawing.Point(63, 73)
        Me.txtbj2_4j.Name = "txtbj2_4j"
        Me.txtbj2_4j.Size = New System.Drawing.Size(100, 20)
        Me.txtbj2_4j.TabIndex = 1
        '
        'pl_3j
        '
        Me.pl_3j.Controls.Add(Me.Label14)
        Me.pl_3j.Controls.Add(Me.Label12)
        Me.pl_3j.Controls.Add(Me.Label13)
        Me.pl_3j.Controls.Add(Me.comB_j3_3j)
        Me.pl_3j.Controls.Add(Me.comB_j1_3j)
        Me.pl_3j.Controls.Add(Me.comB_j2_3j)
        Me.pl_3j.Controls.Add(Me.Label4)
        Me.pl_3j.Controls.Add(Me.Label3)
        Me.pl_3j.Controls.Add(Me.Label5)
        Me.pl_3j.Controls.Add(Me.txtbj3_3j)
        Me.pl_3j.Controls.Add(Me.txtbj2_3j)
        Me.pl_3j.Controls.Add(Me.txtbj1_3j)
        Me.pl_3j.Location = New System.Drawing.Point(284, 123)
        Me.pl_3j.Name = "pl_3j"
        Me.pl_3j.Size = New System.Drawing.Size(164, 251)
        Me.pl_3j.TabIndex = 7
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(3, 40)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(32, 13)
        Me.Label14.TabIndex = 14
        Me.Label14.Text = "Age :"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(3, 160)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(32, 13)
        Me.Label12.TabIndex = 12
        Me.Label12.Text = "Age :"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(3, 98)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(32, 13)
        Me.Label13.TabIndex = 13
        Me.Label13.Text = "Age :"
        '
        'comB_j3_3j
        '
        Me.comB_j3_3j.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comB_j3_3j.FormattingEnabled = True
        Me.comB_j3_3j.Location = New System.Drawing.Point(65, 155)
        Me.comB_j3_3j.Name = "comB_j3_3j"
        Me.comB_j3_3j.Size = New System.Drawing.Size(37, 21)
        Me.comB_j3_3j.TabIndex = 7
        '
        'comB_j1_3j
        '
        Me.comB_j1_3j.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comB_j1_3j.FormattingEnabled = True
        Me.comB_j1_3j.Location = New System.Drawing.Point(63, 37)
        Me.comB_j1_3j.Name = "comB_j1_3j"
        Me.comB_j1_3j.Size = New System.Drawing.Size(37, 21)
        Me.comB_j1_3j.TabIndex = 8
        '
        'comB_j2_3j
        '
        Me.comB_j2_3j.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comB_j2_3j.FormattingEnabled = True
        Me.comB_j2_3j.Location = New System.Drawing.Point(65, 95)
        Me.comB_j2_3j.Name = "comB_j2_3j"
        Me.comB_j2_3j.Size = New System.Drawing.Size(37, 21)
        Me.comB_j2_3j.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 129)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Joueur 3 :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Joueur 1 :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 69)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(54, 13)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Joueur 2 :"
        '
        'txtbj3_3j
        '
        Me.txtbj3_3j.Location = New System.Drawing.Point(63, 126)
        Me.txtbj3_3j.Name = "txtbj3_3j"
        Me.txtbj3_3j.Size = New System.Drawing.Size(100, 20)
        Me.txtbj3_3j.TabIndex = 2
        '
        'txtbj2_3j
        '
        Me.txtbj2_3j.Location = New System.Drawing.Point(63, 67)
        Me.txtbj2_3j.Name = "txtbj2_3j"
        Me.txtbj2_3j.Size = New System.Drawing.Size(100, 20)
        Me.txtbj2_3j.TabIndex = 1
        '
        'txtbj1_3j
        '
        Me.txtbj1_3j.Location = New System.Drawing.Point(63, 11)
        Me.txtbj1_3j.Name = "txtbj1_3j"
        Me.txtbj1_3j.Size = New System.Drawing.Size(100, 20)
        Me.txtbj1_3j.TabIndex = 0
        '
        'Nbr_de_joueurs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(804, 451)
        Me.Controls.Add(Me.pl_3j)
        Me.Controls.Add(Me.pl_4j)
        Me.Controls.Add(Me.pl_2j)
        Me.Controls.Add(Me.btn_retour)
        Me.Controls.Add(Me.btn_validé)
        Me.Controls.Add(Me.btn_j4)
        Me.Controls.Add(Me.btn_j3)
        Me.Controls.Add(Me.btn_j2)
        Me.Name = "Nbr_de_joueurs"
        Me.Text = "Nbr_de_joueurs"
        Me.pl_2j.ResumeLayout(False)
        Me.pl_2j.PerformLayout()
        Me.pl_4j.ResumeLayout(False)
        Me.pl_4j.PerformLayout()
        Me.pl_3j.ResumeLayout(False)
        Me.pl_3j.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btn_j2 As Button
    Friend WithEvents btn_j3 As Button
    Friend WithEvents btn_j4 As Button
    Friend WithEvents btn_validé As Button
    Friend WithEvents btn_retour As Button
    Friend WithEvents pl_2j As Panel
    Friend WithEvents pl_4j As Panel
    Friend WithEvents pl_3j As Panel
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents comB_j2_2j As ComboBox
    Friend WithEvents comB_j1_2j As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtbj2_2j As TextBox
    Friend WithEvents txtbj1_2j As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents txtbj4_4j As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtbj1_4j As TextBox
    Friend WithEvents txtbj2_4j As TextBox
    Friend WithEvents txtbj3_4j As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents comB_j3_3j As ComboBox
    Friend WithEvents comB_j1_3j As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtbj3_3j As TextBox
    Friend WithEvents txtbj2_3j As TextBox
    Friend WithEvents txtbj1_3j As TextBox
    Friend WithEvents comB_j2_3j As ComboBox
    Friend WithEvents comB_j3_4j As ComboBox
    Friend WithEvents comB_j4_4j As ComboBox
    Friend WithEvents comB_j2_4j As ComboBox
    Friend WithEvents comB_j1_4j As ComboBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents ToolTip2 As ToolTip
    Friend WithEvents ToolTip3 As ToolTip
    Friend WithEvents ToolTip4 As ToolTip
    Friend WithEvents ToolTip5 As ToolTip
    Friend WithEvents ToolTip6 As ToolTip
    Friend WithEvents ToolTip7 As ToolTip
    Friend WithEvents ToolTip8 As ToolTip
    Friend WithEvents ToolTip9 As ToolTip
    Friend WithEvents ToolTip10 As ToolTip
End Class
