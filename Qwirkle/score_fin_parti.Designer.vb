﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Score_fin_parti
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lbl_scorej1 = New System.Windows.Forms.Label()
        Me.lbl_j2 = New System.Windows.Forms.Label()
        Me.lbl_scorej2 = New System.Windows.Forms.Label()
        Me.lbl_j3 = New System.Windows.Forms.Label()
        Me.lbl_scorej3 = New System.Windows.Forms.Label()
        Me.lbl_j4 = New System.Windows.Forms.Label()
        Me.lbl_scorej4 = New System.Windows.Forms.Label()
        Me.BtnRejouer = New System.Windows.Forms.Button()
        Me.BtnQuitter = New System.Windows.Forms.Button()
        Me.lbl_j1 = New System.Windows.Forms.Label()
        Me.pl_j3_score = New System.Windows.Forms.Panel()
        Me.pic_j3 = New System.Windows.Forms.PictureBox()
        Me.pl_j4_score = New System.Windows.Forms.Panel()
        Me.pic_j4 = New System.Windows.Forms.PictureBox()
        Me.pic_j2 = New System.Windows.Forms.PictureBox()
        Me.pic_j1 = New System.Windows.Forms.PictureBox()
        Me.pl_j2_score = New System.Windows.Forms.Panel()
        Me.pl_j1_score = New System.Windows.Forms.Panel()
        Me.pl_j3_score.SuspendLayout()
        CType(Me.pic_j3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pl_j4_score.SuspendLayout()
        CType(Me.pic_j4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_j2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_j1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pl_j2_score.SuspendLayout()
        Me.pl_j1_score.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(41, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(0, 13)
        Me.Label1.TabIndex = 0
        '
        'lbl_scorej1
        '
        Me.lbl_scorej1.AutoSize = True
        Me.lbl_scorej1.Location = New System.Drawing.Point(3, 62)
        Me.lbl_scorej1.Name = "lbl_scorej1"
        Me.lbl_scorej1.Size = New System.Drawing.Size(60, 13)
        Me.lbl_scorej1.TabIndex = 1
        Me.lbl_scorej1.Text = "LblScoreJ1"
        '
        'lbl_j2
        '
        Me.lbl_j2.AutoSize = True
        Me.lbl_j2.Location = New System.Drawing.Point(3, 14)
        Me.lbl_j2.Name = "lbl_j2"
        Me.lbl_j2.Size = New System.Drawing.Size(54, 13)
        Me.lbl_j2.TabIndex = 2
        Me.lbl_j2.Text = "LblNomJ2"
        '
        'lbl_scorej2
        '
        Me.lbl_scorej2.AutoSize = True
        Me.lbl_scorej2.Location = New System.Drawing.Point(3, 62)
        Me.lbl_scorej2.Name = "lbl_scorej2"
        Me.lbl_scorej2.Size = New System.Drawing.Size(60, 13)
        Me.lbl_scorej2.TabIndex = 3
        Me.lbl_scorej2.Text = "LblScoreJ2"
        '
        'lbl_j3
        '
        Me.lbl_j3.AutoSize = True
        Me.lbl_j3.Location = New System.Drawing.Point(3, 14)
        Me.lbl_j3.Name = "lbl_j3"
        Me.lbl_j3.Size = New System.Drawing.Size(54, 13)
        Me.lbl_j3.TabIndex = 4
        Me.lbl_j3.Text = "LblNomJ3"
        '
        'lbl_scorej3
        '
        Me.lbl_scorej3.AutoSize = True
        Me.lbl_scorej3.Location = New System.Drawing.Point(3, 62)
        Me.lbl_scorej3.Name = "lbl_scorej3"
        Me.lbl_scorej3.Size = New System.Drawing.Size(60, 13)
        Me.lbl_scorej3.TabIndex = 5
        Me.lbl_scorej3.Text = "LblScoreJ3"
        '
        'lbl_j4
        '
        Me.lbl_j4.AutoSize = True
        Me.lbl_j4.Location = New System.Drawing.Point(3, 14)
        Me.lbl_j4.Name = "lbl_j4"
        Me.lbl_j4.Size = New System.Drawing.Size(54, 13)
        Me.lbl_j4.TabIndex = 6
        Me.lbl_j4.Text = "LblNomJ4"
        '
        'lbl_scorej4
        '
        Me.lbl_scorej4.AutoSize = True
        Me.lbl_scorej4.Location = New System.Drawing.Point(3, 62)
        Me.lbl_scorej4.Name = "lbl_scorej4"
        Me.lbl_scorej4.Size = New System.Drawing.Size(60, 13)
        Me.lbl_scorej4.TabIndex = 7
        Me.lbl_scorej4.Text = "LblScoreJ4"
        '
        'BtnRejouer
        '
        Me.BtnRejouer.Location = New System.Drawing.Point(585, 384)
        Me.BtnRejouer.Name = "BtnRejouer"
        Me.BtnRejouer.Size = New System.Drawing.Size(75, 23)
        Me.BtnRejouer.TabIndex = 8
        Me.BtnRejouer.Text = "Rejouer"
        Me.BtnRejouer.UseVisualStyleBackColor = True
        '
        'BtnQuitter
        '
        Me.BtnQuitter.Location = New System.Drawing.Point(66, 384)
        Me.BtnQuitter.Name = "BtnQuitter"
        Me.BtnQuitter.Size = New System.Drawing.Size(75, 23)
        Me.BtnQuitter.TabIndex = 9
        Me.BtnQuitter.Text = "Quitter"
        Me.BtnQuitter.UseVisualStyleBackColor = True
        '
        'lbl_j1
        '
        Me.lbl_j1.AutoSize = True
        Me.lbl_j1.Location = New System.Drawing.Point(3, 14)
        Me.lbl_j1.Name = "lbl_j1"
        Me.lbl_j1.Size = New System.Drawing.Size(54, 13)
        Me.lbl_j1.TabIndex = 10
        Me.lbl_j1.Text = "LblNomJ1"
        '
        'pl_j3_score
        '
        Me.pl_j3_score.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pl_j3_score.Controls.Add(Me.lbl_scorej3)
        Me.pl_j3_score.Controls.Add(Me.lbl_j3)
        Me.pl_j3_score.Controls.Add(Me.pic_j3)
        Me.pl_j3_score.Location = New System.Drawing.Point(375, 49)
        Me.pl_j3_score.Name = "pl_j3_score"
        Me.pl_j3_score.Size = New System.Drawing.Size(70, 200)
        Me.pl_j3_score.TabIndex = 13
        '
        'pic_j3
        '
        Me.pic_j3.Image = Global.Qwirkle.My.Resources.Resources.coupe
        Me.pic_j3.Location = New System.Drawing.Point(6, 116)
        Me.pic_j3.Name = "pic_j3"
        Me.pic_j3.Size = New System.Drawing.Size(57, 50)
        Me.pic_j3.TabIndex = 15
        Me.pic_j3.TabStop = False
        '
        'pl_j4_score
        '
        Me.pl_j4_score.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pl_j4_score.Controls.Add(Me.lbl_scorej4)
        Me.pl_j4_score.Controls.Add(Me.pic_j4)
        Me.pl_j4_score.Controls.Add(Me.lbl_j4)
        Me.pl_j4_score.Location = New System.Drawing.Point(569, 49)
        Me.pl_j4_score.Name = "pl_j4_score"
        Me.pl_j4_score.Size = New System.Drawing.Size(70, 200)
        Me.pl_j4_score.TabIndex = 14
        '
        'pic_j4
        '
        Me.pic_j4.Image = Global.Qwirkle.My.Resources.Resources.coupe
        Me.pic_j4.Location = New System.Drawing.Point(6, 116)
        Me.pic_j4.Name = "pic_j4"
        Me.pic_j4.Size = New System.Drawing.Size(57, 50)
        Me.pic_j4.TabIndex = 16
        Me.pic_j4.TabStop = False
        '
        'pic_j2
        '
        Me.pic_j2.Image = Global.Qwirkle.My.Resources.Resources.coupe
        Me.pic_j2.Location = New System.Drawing.Point(6, 116)
        Me.pic_j2.Name = "pic_j2"
        Me.pic_j2.Size = New System.Drawing.Size(57, 50)
        Me.pic_j2.TabIndex = 17
        Me.pic_j2.TabStop = False
        '
        'pic_j1
        '
        Me.pic_j1.Image = Global.Qwirkle.My.Resources.Resources.coupe
        Me.pic_j1.Location = New System.Drawing.Point(6, 116)
        Me.pic_j1.Name = "pic_j1"
        Me.pic_j1.Size = New System.Drawing.Size(57, 50)
        Me.pic_j1.TabIndex = 11
        Me.pic_j1.TabStop = False
        '
        'pl_j2_score
        '
        Me.pl_j2_score.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pl_j2_score.Controls.Add(Me.lbl_j2)
        Me.pl_j2_score.Controls.Add(Me.pic_j2)
        Me.pl_j2_score.Controls.Add(Me.lbl_scorej2)
        Me.pl_j2_score.Location = New System.Drawing.Point(218, 49)
        Me.pl_j2_score.Name = "pl_j2_score"
        Me.pl_j2_score.Size = New System.Drawing.Size(70, 200)
        Me.pl_j2_score.TabIndex = 14
        '
        'pl_j1_score
        '
        Me.pl_j1_score.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pl_j1_score.Controls.Add(Me.pic_j1)
        Me.pl_j1_score.Controls.Add(Me.lbl_scorej1)
        Me.pl_j1_score.Controls.Add(Me.lbl_j1)
        Me.pl_j1_score.Controls.Add(Me.Label1)
        Me.pl_j1_score.Location = New System.Drawing.Point(66, 49)
        Me.pl_j1_score.Name = "pl_j1_score"
        Me.pl_j1_score.Size = New System.Drawing.Size(70, 200)
        Me.pl_j1_score.TabIndex = 18
        '
        'Score_fin_parti
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.pl_j1_score)
        Me.Controls.Add(Me.pl_j2_score)
        Me.Controls.Add(Me.pl_j4_score)
        Me.Controls.Add(Me.pl_j3_score)
        Me.Controls.Add(Me.BtnQuitter)
        Me.Controls.Add(Me.BtnRejouer)
        Me.Name = "Score_fin_parti"
        Me.Text = "Score_fin_parti"
        Me.pl_j3_score.ResumeLayout(False)
        Me.pl_j3_score.PerformLayout()
        CType(Me.pic_j3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pl_j4_score.ResumeLayout(False)
        Me.pl_j4_score.PerformLayout()
        CType(Me.pic_j4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_j2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_j1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pl_j2_score.ResumeLayout(False)
        Me.pl_j2_score.PerformLayout()
        Me.pl_j1_score.ResumeLayout(False)
        Me.pl_j1_score.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents lbl_scorej1 As Label
    Friend WithEvents lbl_j2 As Label
    Friend WithEvents lbl_scorej2 As Label
    Friend WithEvents lbl_j3 As Label
    Friend WithEvents lbl_scorej3 As Label
    Friend WithEvents lbl_j4 As Label
    Friend WithEvents lbl_scorej4 As Label
    Friend WithEvents BtnRejouer As Button
    Friend WithEvents BtnQuitter As Button
    Friend WithEvents lbl_j1 As Label
    Friend WithEvents pic_j1 As PictureBox
    Friend WithEvents pl_j3_score As Panel
    Friend WithEvents pl_j4_score As Panel
    Friend WithEvents pic_j3 As PictureBox
    Friend WithEvents pic_j4 As PictureBox
    Friend WithEvents pic_j2 As PictureBox
    Friend WithEvents pl_j2_score As Panel
    Friend WithEvents pl_j1_score As Panel
End Class
