﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleClass
{
    public class Plateau // Constructeur du plateau avec une liste de liste , une liste de tuile
    {
        private List<List<Tuiles>> plateau;
        private List<List<int>> gestion;
        private bool ajoutLigne, ajoutLigneFin, ajoutColonne, ajoutColonneFin = false;
        int debugColonne, debugLigne;
        private bool valide = true;
        private bool premierTour = true;
        private bool deuxiemeTour = false;
        private bool deuxiemePoseBool = false;
        private bool autrePoseBool = false;
        private int stockageLigne = -1;
        private int stockageColonne = -1;


        public Plateau()
        //Le constructeur construit un une liste de liste de tuile de taille 3x3 
        // Ainsi qu'une liste de liste de int de taille 3x3 pour calculer les points 
        
        {
            plateau = new List<List<Tuiles>>();
            gestion = new List<List<int>>();
            for (int colonne = 0; colonne < 3; colonne++)
            {
                plateau.Add(new List<Tuiles>());
                gestion.Add(new List<int>());
                for (int ligne = 0; ligne < 3; ligne++)
                {
                    plateau[colonne].Add(new Tuiles());
                    gestion[colonne].Add(0);
                }
            }
        }

        public Tuiles GetPlateau(int colonne, int ligne)
        {
            return plateau[colonne][ligne]; // Renvoie la tuile qui est à la position[x, y]
        } 
        public void SetPlateau(int ligne, int colonne, Tuiles tuile) 
        {
            plateau[ligne][colonne] = tuile; //Modification de la tuile à la position [x,y]
        }
        public void SetGestion(int ligne, int colonne, int valeur)
        {
            gestion[ligne][colonne] = valeur; // Modifie la valeur en gestion à la position[x][y]. Utilisé pour les Tests Unitaires
        }  
        public int GetGestion(int ligne, int colonne) 
        {
            return gestion[ligne][colonne]; // Récupère la valeur de gestion à la position[x][y]
        }
        public int LargeurPlateau
        {
            get
            {
                return plateau.Count(); // Récupère la taille de la première liste
            }
        }
        public int HauteurPlateau
        {
            get
            {
                return plateau[LargeurPlateau - 2].Count(); // Récupère la taille de la liste de tuile (-2 pour eviter
                                                            // les out of index
            }
        }
        public int LargeurGestion
        {
            get
            {
                return gestion.Count(); // Récupère la taille de la première liste
            }
        }
        public int HauteurGestion
        {
            get
            {
                return gestion[LargeurGestion - 1].Count(); // récupère la taille de la liste de int 
            }
        }
        public bool Valide
        {
            get
            {
                return valide; // Renvoi le booléen valide pour la pose d'une tuile. Utilisé pour les Tests Unitaires
            }
        }
        public bool DeuxiemeTour
        {
            get { return deuxiemeTour; } //Renvoi le booléen deuxiemeTour pour la pose d'une tuile. Utilisé pour les Tests Unitaires
        }
        public bool DeuxiemePoseBool
        {
            get { return deuxiemePoseBool; } //Renvoi le booléen deuxiemePoseBool pour la pose d'une tuile. Utilisé pour les Tests Unitaires
        }
        public bool AutrePoseBool
        {
            get { return autrePoseBool; } // Renvoi le booléen autrePoseBool pour la pose d'une tuile. Utilisé pour les Tests Unitaires
        }
        public bool AjoutColonne
        {
            get { return ajoutColonne; } //Renvoi le booléen ajoutColonne pour la pose d'une tuile. Utilisé pour les Tests Unitaires
        }
        public bool AjoutColonneFin
        {
            get { return ajoutColonneFin; } //Renvoi le booléen ajoutColonneFin pour la pose d'une tuile. Utilisé pour les Tests Unitaires
        }
        public bool AjoutLigne
        {
            get { return ajoutLigne; } //Renvoi le booléen ajoutLigne pour la pose d'une tuile. Utilisé pour les Tests Unitaires
        }
        public bool AjoutLigneFin
        {
            get { return ajoutLigneFin; } // Renvoi le booléen ajoutLigneFin pour la pose d'une tuile. Utilisé pour les Tests Unitaires
        }
        public int StockageLigne
        {
            get { return stockageLigne; } // Renvoi le booléen stockageLigne pour la pose d'une tuile. Utilisé pour les Tests Unitaires
        }
        public int StockageColonne
        {
            get { return stockageColonne; } // Renvoi le booléen stockageColonne pour la pose d'une tuile. Utilisé pour les Tests Unitaires
        }
        public void AddColonne(bool fin)
        {
           
            if (fin == true) // Vérifie si l'ajout de colonne se situe à la fin ou non
            {
                plateau.Add(new List<Tuiles>());
                gestion.Add(new List<int>());
                for (int i = 0; i < this.HauteurPlateau; i++)
                {

                    plateau[this.LargeurPlateau - 1].Add(new Tuiles());
                    gestion[this.LargeurGestion - 1].Add(0);
                }
                ajoutColonneFin = true; // Ce booléen est utilisé en cas de pose de tuile non valide.
            }
            else // ajout d'une colonne en début de liste
            {
                plateau.Insert(0, new List<Tuiles>());
                gestion.Insert(0, new List<int>());
                for (int i = 0; i < this.HauteurPlateau; i++)
                {
                    plateau[0].Add(new Tuiles());
                    gestion[0].Add(0);
                }
                ajoutColonne = true; // Utilisé en cas de pose de tuile non valide
            }
        }
        public void AddLigne(bool fin) //Même chose que AddColonne mais pour les lignes
        {
            if (fin)
            {
                for (int iteration = 0; iteration < this.LargeurPlateau; iteration++)
                {
                    plateau[iteration].Add(new Tuiles());
                    gestion[iteration].Add(0);
                }
                ajoutLigneFin = true;
            }
            else
            {
                for (int iteration = 0; iteration < this.LargeurPlateau; iteration++)
                {
                    plateau[iteration].Insert(0, new Tuiles());
                    gestion[iteration].Insert(0, 0);
                }
                ajoutLigne = true;
            }
        }
        public void Redimension(int colonne, int ligne) // Redimension vérifie si le tableau a besoin d'être Redimensionné 
                                              // ou non et à quel endroit
        {
            if (colonne == 0) // si début de liste
            {
                AddColonne(fin: false);
                colonne++;
            }
            if (colonne == LargeurPlateau - 1) // si fin de liste 
            {
                AddColonne(fin: true);
            }
            if (ligne == 0) // Si début de Liste
            {
                AddLigne(fin: false);
                ligne++;

            }
            if (ligne == HauteurPlateau - 1) // Si fin de Liste
            {
                AddLigne(fin: true);
            }
            debugColonne = colonne; // Cette variable est utilisé pour deboguer la fonction car elle ne renvoi pas la nouvelle valeur de X et Y 
            debugLigne = ligne;
        }
        public void PositionSelect(int colonne, int ligne) // Verifie qu'une tuile n'est pas déjà existante 
                                                 // a l'emplacement x,y
        {
            if (plateau[colonne][ligne].Couleur != 6 && plateau[colonne][ligne].Forme != 6) // 6 représentant la tuile "vide"
            {

                valide = false; // renvoi false avec AddTuile() si pose impossible
            }
        }
        public void VerificationCote(int colonne, int ligne, Tuiles tuiles)
        {
            //Cette fonctionne s'excute après avoir vérifier si une pose est possible 
            // il va vérifier le coté Droit,Gauche,Bas et Haut pour savoir si la tuile est posable

            //Verification droit
            if (!(plateau[colonne + 1][ligne].Couleur == 6 && plateau[colonne + 1][ligne].Forme == 6) && // On regarde si l'emplacement contient déjà une tuile
                !(plateau[colonne + 1][ligne].Couleur == tuiles.Couleur && plateau[colonne + 1][ligne].Forme != tuiles.Forme || // vérifie que les tuiles ont bien la même couleur et pas la même forme
                  plateau[colonne + 1][ligne].Couleur != tuiles.Couleur && plateau[colonne + 1][ligne].Forme == tuiles.Forme)) // vérifie que les tuiles ont bien la même forme et pas la même couleur
            {
                valide = false; // false si une des conditions n'est pas bonne pour poser la tuile
            }
            // Vérification gauche
            if (!(plateau[colonne - 1][ligne].Couleur == 6 && plateau[colonne - 1][ligne].Forme == 6) &&
                !(plateau[colonne - 1][ligne].Couleur == tuiles.Couleur && plateau[colonne - 1][ligne].Forme != tuiles.Forme ||
                  plateau[colonne - 1][ligne].Couleur != tuiles.Couleur && plateau[colonne - 1][ligne].Forme == tuiles.Forme))
            {
                valide = false;
            }
            // Vérification Haut
            if (!(plateau[colonne][ligne + 1].Couleur == 6 && plateau[colonne][ligne + 1].Forme == 6) && 
                !(plateau[colonne][ligne + 1].Couleur == tuiles.Couleur && plateau[colonne][ligne + 1].Forme != tuiles.Forme ||
                  plateau[colonne][ligne + 1].Couleur != tuiles.Couleur && plateau[colonne][ligne + 1].Forme == tuiles.Forme))
            {
                valide = false;
            }
            //Vérification Bas
            if (!(plateau[colonne][ligne - 1].Couleur == 6 && plateau[colonne][ligne - 1].Forme == 6) && !(plateau[colonne][ligne - 1].Couleur == tuiles.Couleur && plateau[colonne][ligne - 1].Forme != tuiles.Forme ||
                    plateau[colonne][ligne - 1].Couleur != tuiles.Couleur && plateau[colonne][ligne - 1].Forme == tuiles.Forme))
            {
                valide = false;
            }
            // renvoi false si la pose est impossible
        }
        public void VerificationTuileUnique(int colonne, int ligne, Tuiles tuiles)
        {
            int position = 1;
            if (gestion[colonne + 1][ligne] != 0) //Vérifie sur le coté droit si une tuile est posée
            {
                while (gestion[colonne + position][ligne] != 0) // continue de parcourirs les tuiles sur le coté droit si il y'a 
                {
                    if (plateau[colonne + position][ligne] == tuiles) // Si on renconte la même tuile que celle qu'on pose , impossible d'ajouter la tuile
                    {
                        valide = false;
                    }
                    position++;
                }
            }
            position = 1;
            //Même chose mais pour le coté gauche 
            if (gestion[colonne - 1][ligne] != 0)
            {
                while (gestion[colonne - position][ligne] != 0)
                {
                    if (plateau[colonne - position][ligne] == tuiles)
                    {
                        valide = false;
                    }
                    position++;
                }
            }
            position = 1;
            //Même chose mais pour le coté haut
            if (gestion[colonne][ligne + 1] != 0)
            {
                while (gestion[colonne][ligne + position] != 0)
                {
                    if (plateau[colonne][ligne + position] == tuiles)
                    {
                        valide = false;
                    }
                    position++;
                }
            }
            position = 1;
            //Même chose mais pour le coté bas
            if (gestion[colonne][ligne - 1] != 0)
            {
                while (gestion[colonne][ligne - position] != 0)
                {
                    if (plateau[colonne][ligne - position] == tuiles)
                    {
                        valide = false;
                    }
                    position++;
                }
            }
            position = 1;
        }
        public void VerificationIdentique(int colonne, int ligne, Tuiles tuiles)
        {   //Cette methode vérifie si toute les tuiles sont les même, pour éviter de changer de couleurs entre deux tuiles
            if (gestion[colonne + 1][ligne] != 0 && plateau[colonne + 2][ligne].Couleur != tuiles.Couleur && 
                plateau[colonne + 2][ligne].Forme != tuiles.Forme && plateau[colonne + 2][ligne].Couleur != 6 && plateau[colonne + 2][ligne].Forme != 6) // vérifie que la tuile 2 case a droite n'est pas vide et si elle elle est du
            {                                                                                                              // même type que la tuile que nous souhaitons poser
                valide = false;
            }
            // même chose pour coté gauche
            if (gestion[colonne - 1][ligne] != 0)
            {
                if (gestion[colonne - 1][ligne] != 0 && plateau[colonne - 2][ligne].Couleur != tuiles.Couleur &&
                    plateau[colonne - 2][ligne].Forme != tuiles.Forme && plateau[colonne - 2][ligne].Couleur != 6 && plateau[colonne - 2][ligne].Forme != 6)
                {
                    valide = false;
                }
            }
            // même chose pour coté haut
            if (gestion[colonne][ligne + 1] != 0)
            {
                if (gestion[colonne][ligne + 1] != 0 && plateau[colonne][ligne + 2].Couleur != tuiles.Couleur && 
                    plateau[colonne][ligne + 2].Forme != tuiles.Forme && plateau[colonne][ligne + 2].Couleur != 6 && plateau[colonne][ligne + 2].Forme != 6)
                {
                    valide = false;
                }
            }
            // même chose pour coté Bas
            if (gestion[colonne][ligne - 1] != 0)
            {
                if (gestion[colonne][ligne - 1] != 0 && plateau[colonne][ligne - 2].Couleur != tuiles.Couleur && 
                    plateau[colonne][ligne - 2].Forme != tuiles.Forme && plateau[colonne][ligne - 2].Couleur != 6 && plateau[colonne][ligne - 2].Forme != 6)
                {
                    valide = false;
                }
            }
        }
        public bool AddTuile(int colonne, int ligne, Tuiles tuile)
        {
            Redimension(colonne, ligne); // Ajuste le tableau pour poser la tuile
            if (debugColonne != colonne) colonne = debugColonne; // Dans la cas ou la redimension est necessaire X et Y prenne les nouvelles
            if (debugLigne != ligne) ligne = debugLigne; // valeurs pour les  replacer au bonne endroit des les Listes
            PositionSelect(colonne, ligne); // vérifie si il n'y pas une tuile déjà présente
            VerificationCote(colonne, ligne, tuile); //Vérifie les cotés pour voir si la pose est possible
            VerificationTuileUnique(colonne, ligne, tuile); // Vérifie si il n'y pas deux fois la même tuile 
            VerificationIdentique(colonne, ligne, tuile);
            if (premierTour == false)
            {
                if (deuxiemeTour == false && deuxiemePoseBool == false && autrePoseBool == false) // Bloque le joueur et l'oblige a poser une tuile à coté de celle qu'il viens de placer
                {
                    DeuxiemePose(colonne, ligne);
                    deuxiemePoseBool = true;

                }
                if (deuxiemeTour == false && deuxiemePoseBool == true && autrePoseBool == false) // Bloque le joueurs sur une ligne ou sur une colonne en fonction de comment il à posé les tuiles
                {
                    SauvegardePose();
                    AutrePose(colonne, ligne);

                }

                if (deuxiemeTour == true && deuxiemePoseBool == false && autrePoseBool == false) // Oblige le joueurs à placer a coté d'une tuile déjà posé
                {
                    PremierePose(colonne, ligne);
                    deuxiemeTour = false;
                }
            }
            if (valide == true) // si la pose est possible 
            {
                plateau[colonne][ligne] = tuile; // ajoute la tuile
                gestion[colonne][ligne] = 2; // Prends la valeur 2 pour le calcul des score
                ResetBoolDimension(); // Reset les booléens 
                premierTour = false; // permet de bloquer la pose de tuile n'importe ou sur le plateau 
                return true;
            }
            else
            {
                SuppressionColonneLigne(); // supprime la ligne et la colonne ajouté si la tuile n'a pas été posé 
                ResetBoolNonValide(); // Reset des booléens 
                valide = true;
                return false; // tuile non posé

            }

        }
        public void ResetBool() //Reset les booléens utilisé pour les tours 
        {
            deuxiemeTour = true;
            deuxiemePoseBool = false;
            autrePoseBool = false;
        }
        public void ResetBoolNonValide() // Reset les booléens pour les tours en cas de tuile non posé
        {
            if (deuxiemeTour == false && deuxiemePoseBool == false && autrePoseBool == false)
            {
                deuxiemeTour = true;
            }
            if (deuxiemeTour == false && deuxiemePoseBool == true && autrePoseBool == false)
            {

                deuxiemePoseBool = false;

            }
        }
        public void SauvegardePose() // Utilisé pour bloqué la pose du joueurs sur une ligne ou une colonne
        {
            for (int colonne = 0; colonne < LargeurGestion; colonne++)
            {
                for (int ligne = 0; ligne < HauteurGestion; ligne++)
                {
                    if (gestion[colonne][ligne] == 2)
                    {
                        if (gestion[colonne + 1][ligne] == 2)
                        {
                            stockageLigne = ligne;
                        }
                        if (gestion[colonne][ligne + 1] == 2)
                        {
                            stockageColonne = colonne;
                        }

                    }
                }
            }
        }
        public void AutrePose(int colonne, int ligne) // Utilisé pour bloqué la pose du joueurs sur une ligne ou colonne 
        {
            if (stockageLigne != -1 && stockageLigne != ligne)
            {
                valide = false;
            }
            if (stockageColonne != -1 && stockageColonne != colonne)
            {
                valide = false;
            }
        }
        public void DeuxiemePose(int colonne, int ligne) // Force le joueur a poser sa tuile après à coté de sa première tuile 
        {
            if (gestion[colonne + 1][ligne] != 2 && gestion[colonne - 1][ligne] != 2 && gestion[colonne][ligne + 1] != 2 && gestion[colonne][ligne - 1] != 2)
            {
                valide = false;
            }
        }
        public void PremierePose(int colonne, int ligne) //Force le joueur à poser une tuile à coté d'une tuile déjà posé 
        {
            if (gestion[colonne + 1][ligne] != 1 && gestion[colonne - 1][ligne] != 1 && gestion[colonne][ligne + 1] != 1 && gestion[colonne][ligne - 1] != 1)
            {
                valide = false;
            }
        }
        public void ResetBoolDimension() //Reset des booléens
        {
            ajoutColonne = false;
            ajoutColonneFin = false;
            ajoutLigne = false;
            ajoutLigneFin = false;
            stockageColonne = -1;
            stockageLigne = -1;

        }
        public void SuppressionColonneLigne() // Utilise les booléens stocké plus haut pour supprimer les lignes et colonnes si nécessaire
        {
            if (ajoutColonne == true)
            {
                plateau.RemoveAt(0);
                gestion.RemoveAt(0);
                ajoutColonne = false;
            }
            if (ajoutColonneFin == true)
            {
                plateau.RemoveAt(LargeurPlateau - 1);
                gestion.RemoveAt(LargeurGestion - 1);
                ajoutColonneFin = false;
            }
            if (ajoutLigne == true)
            {
                for (int colonne = 0; colonne < LargeurPlateau; colonne++)
                {
                    gestion[colonne].RemoveAt(0);
                    plateau[colonne].RemoveAt(0);
                }
                ajoutLigne = false;
            }
            if (ajoutLigneFin == true)
            {
                for (int colonne = 0; colonne < LargeurPlateau; colonne++)
                {
                    plateau[colonne].RemoveAt(HauteurPlateau - 1);
                    gestion[colonne].RemoveAt(HauteurGestion - 1);
                }
                ajoutLigneFin = false;
            }
        }
        public void ArchiveTuile()
        // Cette fonction est utilisé après avoir calculé les points 
        // elle remplace les 2 de des liste Gestion pour les modifier en 1
        {
            for (int colonne = 0; colonne < gestion.Count; colonne++)
            {
                for (int ligne = 0; ligne < gestion[colonne].Count; ligne++)
                {
                    if (gestion[colonne][ligne] == 2) gestion[colonne][ligne] = 1;
                }
            }
        }
        public int ScoreCalcul() // pour plus d'explication se référer au compte rendu 
        {

            int score = 0;
            int position = 1;
            bool Horizontal = false;
            bool Vertical = false;
            int SavePositionX = -1;
            int SavePositionY = -1;
            for (int colonne = 0; colonne < gestion.Count; colonne++)
            {
                for (int ligne = 0; ligne < gestion[colonne].Count; ligne++)
                {
                    if (gestion[colonne][ligne] == 2 && gestion[colonne + 1][ligne] == 2)
                    {
                        Horizontal = true;
                        SavePositionX = colonne;
                        SavePositionY = ligne;
                    }
                    if (gestion[colonne][ligne] == 2 && gestion[colonne][ligne + 1] == 2)
                    {
                        Vertical = true;
                        SavePositionX = colonne;
                        SavePositionY = ligne;
                    }
                }

            }
            if (Horizontal == false && Vertical == false)
            {
                for (int colonne = 0; colonne < gestion.Count; colonne++)
                {
                    for (int ligne = 0; ligne < gestion[colonne].Count; ligne++)
                    {
                        if (gestion[colonne][ligne] == 2)
                        {
                            score++;
                            if (gestion[colonne][ligne - 1] == 1)
                            {
                                while (gestion[colonne][ligne - position] == 1)
                                {
                                    score++;
                                    position++;
                                }
                                position = 1;
                            }
                            if (gestion[colonne][ligne + 1] == 1)
                            {
                                while (gestion[colonne][ligne + position] == 1)
                                {
                                    score++;
                                    position++;
                                }
                                position = 1;
                            }
                            if (gestion[colonne - 1][ligne] == 1)
                            {
                                while (gestion[colonne - position][ligne] == 1)
                                {
                                    score++;
                                    position++;
                                }
                                position = 1;
                            }
                            if (gestion[colonne + position][ligne] == 1)
                            {
                                while (gestion[colonne + position][ligne] == 1)
                                {
                                    score++;
                                    position++;
                                }
                                position = 1;
                            }
                        }

                    }

                }
            }
            if (Horizontal == true)
            {
                score++;
                if (gestion[SavePositionX - 1][SavePositionY] == 1)
                {
                    while (gestion[SavePositionX - position][SavePositionY] == 1)
                    {
                        position++;
                        score++;
                    }
                    position = 1;
                }
                if (gestion[SavePositionX + 1][SavePositionY] != 0)
                {
                    while (gestion[SavePositionX - position][SavePositionY] != 0)
                    {
                        position++;
                        score++;
                    }
                    position = 1;
                }
                for (int colonne = 0; colonne < gestion.Count; colonne++)
                {
                    for (int ligne = 0; ligne < gestion[colonne].Count; ligne++)
                    {
                        if (gestion[colonne][ligne] == 2)
                        {
                            if (gestion[colonne][ligne - 1] == 1 || gestion[colonne][ligne + 1] == 1)
                            {
                                score++;
                            }
                            if (gestion[colonne][ligne - 1] == 1)
                            {
                                while (gestion[colonne][ligne - position] == 1)
                                {
                                    score++;
                                    position++;
                                }
                                position = 1;
                            }
                            if (gestion[colonne][ligne + 1] == 1)
                            {
                                while (gestion[colonne][ligne + position] == 1)
                                {
                                    score++;
                                    position++;
                                }
                                position = 1;
                            }
                        }
                    }

                }

            }
            if (Vertical == true)
            {
                score++;
                if (gestion[SavePositionX][SavePositionY - 1] == 1)
                {
                    while (gestion[SavePositionX][SavePositionY - position] == 1)
                    {
                        position++;
                        score++;
                    }
                    position = 1;
                }
                if (gestion[SavePositionX][SavePositionY + 1] != 0)
                {
                    while (gestion[SavePositionX][SavePositionY + position] != 0)
                    {
                        position++;
                        score++;
                    }
                    position = 1;
                }
                for (int colonne = 0; colonne < gestion.Count; colonne++)
                {
                    for (int ligne = 0; ligne < gestion[colonne].Count; ligne++)
                    {
                        if (gestion[colonne][ligne] == 2)
                        {
                            if (gestion[colonne - 1][ligne] == 1 || gestion[colonne + 1][ligne] == 1)
                            {
                                score++;
                            }
                            if (gestion[colonne - 1][ligne] == 1)
                            {
                                while (gestion[colonne - position][ligne] == 1)
                                {
                                    score++;
                                    position++;
                                }
                                position = 1;
                            }
                            if (gestion[colonne + position][ligne] == 1)
                            {
                                while (gestion[colonne + position][ligne] == 1)
                                {
                                    score++;
                                    position++;
                                }
                                position = 1;
                            }
                        }
                    }

                }

            }
            return score;
        }

        public int ScoreQwirkle()
        {
            // pour plus d'explication se référer au compte rendu 
            int score = 0;
            bool Horizontal = false;
            bool Vertical = false;
            int position = 1;
            int qwirkle = 0;
            for (int colonne = 0; colonne < gestion.Count; colonne++)
            {
                for (int ligne = 0; ligne < gestion[colonne].Count; ligne++) 
                {
                    if (Horizontal == false && Vertical == false) 
                    {
                        if (gestion[colonne][ligne] == 2)
                        {
                            if (gestion[colonne + 1][ligne] == 2 && gestion[colonne - 1][ligne] != 2) Horizontal = true;
                            if (gestion[colonne][ligne + 1] == 2 && gestion[colonne][ligne - 1] != 2) Vertical = true; 
                            if (Horizontal == true)
                            {
                                qwirkle++;
                                if (gestion[colonne - 1][ligne] == 1)
                                {
                                    while (gestion[colonne - position][ligne] == 1)
                                    {
                                        qwirkle++;
                                        position++;
                                    }
                                    position = 1;
                                }
                                if (gestion[colonne + 1][ligne] == 1 || gestion[colonne + 1][ligne] == 2)
                                {
                                    while (gestion[colonne + position][ligne] == 1 || gestion[colonne + position][ligne] == 2)
                                    {
                                        qwirkle++;
                                        position++;
                                    }
                                    position = 1;
                                }
                                if (qwirkle == 6)
                                {
                                    score += 6;
                                    qwirkle = 0;
                                }
                            }
                            if (Vertical == true)
                            {
                                qwirkle++;
                                if (gestion[colonne][ligne - 1] == 1)
                                {
                                    while (gestion[colonne][ligne - position] == 1)
                                    {
                                        qwirkle++;
                                        position++;
                                    }
                                    position = 1;
                                }
                                if (gestion[colonne][ligne + 1] == 1 || gestion[colonne][ligne + 1] == 2)
                                {
                                    while (gestion[colonne][ligne + position] == 1 || gestion[colonne][ligne + position] == 2)
                                    {
                                        qwirkle++;
                                        position++;
                                    }
                                    position = 1;
                                }
                                if (qwirkle == 6)
                                {
                                    score += 6;
                                    qwirkle = 0;
                                }
                            }
                        }
                    }
                    if (Horizontal == true)
                    {
                        if (gestion[colonne][ligne] == 2)
                        {


                            qwirkle++;
                            if (gestion[colonne][ligne - 1] == 1)
                            {
                                while (gestion[colonne][ligne - position] == 1)
                                {
                                    qwirkle++;
                                    position++;
                                }
                                position = 1;
                            }
                            if (gestion[colonne][ligne + 1] == 1)
                            {
                                while (gestion[colonne][ligne + position] == 1)
                                {
                                    qwirkle++;
                                    position++;
                                }
                                position = 1;
                            }
                            if (qwirkle == 6)
                            {
                                score += 6;

                            }
                            qwirkle = 0;
                        }

                    }
                    if (Vertical == true)
                    {
                        if (gestion[colonne][ligne] == 2)
                        {


                            qwirkle++;
                            if (gestion[colonne - 1][ligne] == 1)
                            {
                                while (gestion[colonne - position][ligne] == 1)
                                {
                                    qwirkle++;
                                    position++;
                                }
                                position = 1;
                            }
                            if (gestion[colonne + 1][ligne] == 1)
                            {
                                while (gestion[colonne + position][ligne] == 1)
                                {
                                    qwirkle++;
                                    position++;
                                }
                                position = 1;
                            }
                            if (qwirkle == 6)
                            {
                                score += 6;

                            }
                            qwirkle = 0;
                        }

                    }
                }
            }
            return score;
        }
    }
}
