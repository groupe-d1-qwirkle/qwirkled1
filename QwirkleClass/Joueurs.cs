﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleClass
{
    public class Joueurs
    {
        private string pseudo; //Declaration des variables en private
        private int age;
        private int score;
        private List<Tuiles> main;
        public Joueurs(string pseudo,int age,int score) //Constructeur de la classe joueurs avec pseudo, age, score
        {
            this.pseudo = pseudo;
            this.age = age;
            this.score = score;
            main = new List<Tuiles>();
        }
        public string GetPseudo() //Permet de retourner le pseudo du joueur
        {
            return this.pseudo;
        }
        public List<Tuiles> Getlist() //Retourne les tuiles du joueurs
        {
            return this.main;
        }
        public void DeletItemmain(int index)
        {
            this.main.RemoveAt(index);
        }
        public int CompteurMain()
        {
            return this.main.Count;
        }
        public Tuiles GetMainIndex(int index)
        {
            
            return this.main[index];
        }
        public int  GetAge() //Retourne l'age du joueur
        {
            return this.age;
        }
        public int GetScore() //Retourne le score du joueur
        {
            return this.score;
        }
        public void IncrementationScore(int score) //Incremente le score du joueur avec celui du tour précédent
        { 
            this.score += score;
        }
        public void TuilesManquantes(Sac sacoche)
        {
            int cptManquant; // Declare le nombre de tuiles manquantes
            cptManquant = main.Count(); //Compte le nombre de tuiles dans la main du joueur
            cptManquant = 6 - cptManquant; //Obtient le nombre de tuiles qu'il manque au joueur
            for (int i = 0; i < cptManquant; i++)
            {
                System.Threading.Thread.Sleep(300);
                main.Add(sacoche.PickRandom()); // et pour le nombre de tuiles qu'il lui manque on va lui rendre des tuiles aléatoires (jusqu'à 6)
            }
        }
    }
    
}
