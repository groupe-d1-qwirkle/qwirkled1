﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleClass
{
    public class Sac
    {
        // Sacoche est un tableau qui contient le nombre restant de tuiles de chaque forme et 
        // couleurs possible
        private int[,] Sacoche = new int[6,6];
        
        //Constructeur du sac avec chaque valeurs à 3
        public Sac()
        {
            for(int ligne=0;ligne<6;ligne++)
            {
                for (int colonne = 0; colonne <6; colonne++)
                {
                    this.Sacoche[ligne,colonne] = 3;
                }
            }
        }
        // Methode pour obtenir une valeur dans le sac. Utilisé dans les tests Unitaires
        public int GetSac(int ligne , int colonne)
        {
            return this.Sacoche[ligne,colonne];
        }
        //Modifie le sac pour les Tests Unitaires
        public void SetSac(int ligne, int colonne, int valeur )
        {
            Sacoche[ligne, colonne] = valeur;
        }
        // Vérifie qu'il y'a encore une tuile disponible dans le sac
        public bool IsNotEmpty()
        {
            for (int ligne = 0; ligne < 6; ligne++)
            {
                for (int colonne = 0; colonne < 6; colonne++)
                {

                    if (Sacoche[ligne, colonne] != 0) return true;
                }
            }
            return false;
        }
        // Rajoute une valeur dans le sac quand une tuile est ajouté dans le sac
        public void PutTuiles(int ligne, int colonne)
        {
            Sacoche[ligne, colonne]++;
            
        }
        //Même methode mais avec surcharge
        public void PutTuiles(Tuiles tuile)
        {
            Sacoche[tuile.Couleur, tuile.Forme]++;
        }
        // Generation aléatoire d'une tuile 
        public Tuiles PickRandom()
        {
            if (IsNotEmpty() == true)
            {
                int couleur,forme;
                Random aleatoire = new Random();
                do
                {
                    couleur = aleatoire.Next(6);
                    forme = aleatoire.Next(6);
                }
                while (Sacoche[couleur, forme] == 0);
                Tuiles tuiles = new Tuiles(couleur, forme);
                RemoveTuiles(couleur,forme);
                return tuiles;
            }
            else
            {
                return null;
            }
        }
        // Enleve une valeur dans le sac quand on recupère une tuile.
        public void RemoveTuiles(int ligne,int colonne)
        {
            Sacoche[ligne, colonne] --;
            
        }
        // Même methode avec surcharge 
        public void RemoveTuiles(Tuiles tuiles)
        {
            Sacoche[tuiles.Forme, tuiles.Couleur]--;
        }
        
    }
}
