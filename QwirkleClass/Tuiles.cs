﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleClass
{
    public class Tuiles
    {
        private int couleur;
        private int forme;

        public Tuiles(int couleur, int forme)
        {
            // En cas de x et y non compris dans l'intervalle, x,y=6,6 (tuile null)
            if (couleur < 0 || couleur > 5) { couleur = 6; forme = 6; }
            if (forme < 0 || forme > 5) { couleur = 6; forme = 6; }
            this.couleur = couleur;
            this.forme = forme;
        }
        public Tuiles() : this(6, 6)
        {

        }
        //retourne la couleur d'une tuile
        public int Couleur
        {
            get
            {
                return this.couleur;
            }
            
        }
        // retourne la forme d'une tuile
        public int Forme
        {
            get
            {
                return this.forme;
            }
        }
        //Permet de generer un HashCode pour comparer 2 tuiles
        public override int GetHashCode()
        {
            int hash;
            int.TryParse(string.Format("{0}{1}", this.couleur, this.forme), out hash);
            return hash;
        }
        //surcharge de l'opérateur ==
        public static bool operator ==(Tuiles tuile1, Tuiles tuile2)
        {

            if (tuile1.GetHashCode() == tuile2.GetHashCode())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        // surcharge de l'opérateur !=
        public static bool operator !=(Tuiles tuile1, Tuiles tuile2)
        {

            return !(tuile1 == tuile2);
        }
        // surcharge de l'opérateur Equals
        public override bool Equals(object obj)
        {
            return obj is Tuiles && this == (Tuiles)obj;
        }
    }
}
